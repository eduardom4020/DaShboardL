# server.py
from flask import Flask, render_template, make_response, request, jsonify, send_file
import random

app = Flask(__name__, static_folder="../static/dist", template_folder="../static")

@app.route('/', methods=['GET'])
@app.route('/test', methods=['GET'])
@app.route('/function1', methods=['GET'])
@app.route('/function2', methods=['GET'])
@app.route('/function3', methods=['GET'])
def index():
	return render_template('index.html')

@app.route('/data_var1', methods=['POST'])
def data_var1():
	return make_response(jsonify({'value': [[0, 49.9], [1, 71.5], [2, 106.4], [3, 129.2]]}), 200)

@app.route('/data_var2', methods=['POST'])
def data_var2():
	return make_response(jsonify({'value': [[0, 83.6], [1, 78.8], [2, 98.5], [3, 93.4]]}), 200)

@app.route('/data_var3', methods=['POST'])
def data_var3():
	return make_response(jsonify({'value': [[0, 48.9], [1, 38.8], [2, 39.3], [3, 41.4]]}), 200)

@app.route('/data_var4', methods=['POST'])
def data_var4():
	return make_response(jsonify({'value': [[0, 42.4], [1, 33.2], [2, 34.5], [3, 39.7]]}), 200)

@app.route('/action_function1', methods=['POST'])
def action_function1():
	return make_response(jsonify({'code': 'redirect', 'route': '/function1'}, 200))

@app.route('/action_function2', methods=['POST'])
def action_function2():
	return make_response(jsonify({'code': 'redirect', 'route': '/function2'}, 200))

@app.route('/action_function3', methods=['POST'])
def action_function3():
	return make_response(jsonify({'code': 'redirect', 'route': '/function3'}, 200))

if __name__ == "__main__":
	app.run(debug=True)