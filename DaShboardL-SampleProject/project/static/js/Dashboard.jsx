import React from 'react';
var $ = require('jquery');

import Menu from './Menu';
import BaseChart from './BaseChart';

import { 
	Container

} from 'reactstrap';

const function_result = res => {
	if(res[1] == 200) {
		switch(res[0].code) {
			case 'redirect':
				window.location = res[0].route;
			break;
		}
	}
};

const main_page = <Container>
			<Container>
					<Menu
						style='dropdown'
						name='Menu 1'

						functions = {
							{
								'function1': {
									'action': () => $.post(location.origin + '/action_function1').done(function_result)
								},
								'function2': {
									'action': () => $.post(location.origin + '/action_function2').done(function_result)
								},
								'function3': {
									'action': () => $.post(location.origin + '/action_function3').done(function_result)
								}
							}
						}
					/>
				</Container>	

				<Container>
					<BaseChart
						title='Sample Title'
						subtitle='Sample Subtitle'
						type='column'
						id='chart1'

						series = {
							{
								'var1': {
									'type': 'column',
									'get_data': () => $.post(location.origin + '/data_var1')
								},
								'var2': {
									'type': 'scatter',
									'get_data': () => $.post(location.origin + '/data_var2')
								},
								'var3': {
									'type': 'line',
									'get_data': () => $.post(location.origin + '/data_var3')
								},
								'var4': {
									'type': 'line',
									'get_data': () => $.post(location.origin + '/data_var4')
								}
							}
						}
						
						xAxis = {
							{
								'min': 0,
								'max': 3,
								'labels': [	'Class 1',
											'Class 2',
											'Class 3',
											'Class 4'
										],
								'title': 'xAxis (measure)'
							}
						}

						yAxis = {
							{
								'min': 0,
								'max': 220,
								'title': 'yAxis (measure)'
							}
						}
					/>
			</Container>
</Container>;

const function1_page = <Container>
				<Container>
					<BaseChart
						title='Sample Title'
						subtitle='Sample Subtitle'
						type='column'
						id='chart1'

						series = {
							{
								'var1': {
									'type': 'column',
									'get_data': () => $.post(location.origin + '/data_var1')
								},
								'var2': {
									'type': 'scatter',
									'get_data': () => $.post(location.origin + '/data_var2')
								},
								'var3': {
									'type': 'line',
									'get_data': () => $.post(location.origin + '/data_var3')
								},
								'var4': {
									'type': 'line',
									'get_data': () => $.post(location.origin + '/data_var4')
								}
							}
						}
						
						xAxis = {
							{
								'min': 0,
								'max': 3,
								'labels': [	'Class 1',
											'Class 2',
											'Class 3',
											'Class 4'
										],
								'title': 'xAxis (measure)'
							}
						}

						yAxis = {
							{
								'min': 0,
								'max': 220,
								'title': 'yAxis (measure)'
							}
						}
					/>
			</Container>
</Container>;

const function2_page = <Container>
				<h1>Function 2</h1>
</Container>;

const function3_page = <Container>
				<h1>Function 3</h1>
</Container>;

export default class Dashboard extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			main: main_page,
			function1: function1_page,
			function2: function2_page,
			function3: function3_page
		};
	}

	render() {
		return(this.state[this.props.page]);
	}
}