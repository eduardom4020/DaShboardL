require('../css/app.css');
var $ = require('jquery');

import React from 'react';
import { Router, Route, hashHistory, Switch } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import Dashboard from './Dashboard';

const newHistory = createBrowserHistory();

const main_page = () => <Dashboard page="main"/>;
const function1_page = () => <Dashboard page="function1"/>;
const function2_page = () => <Dashboard page="function2"/>;
const function3_page = () => <Dashboard page="function3"/>;
const test = () => <h1>TESTANDO</h1>;

export default class Project extends React.Component {
	constructor(props) {
		super(props);
	}

	render () {
		return (
			<Router history={newHistory}>
				<Switch>
					<Route path='/test' component={test} />
					<Route path='/function1' component={function1_page} />
					<Route path='/function2' component={function2_page} />
					<Route path='/function3' component={function3_page} />
					<Route path='/' component={main_page} />
				</Switch>
			</Router>
		);
	}
}