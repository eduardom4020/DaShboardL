import React from 'react';
import ReactDOM from 'react-dom';
import Project from './Project';

import { Provider } from 'react-redux';

import 'bootstrap/dist/css/bootstrap.css';

import store from '../redux/store/index';

ReactDOM.render(
	<Provider store={store}>
		<Project />
	</Provider>
, document.getElementById('content'));