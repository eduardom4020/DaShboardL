import React from 'react';
var $ = require('jquery');
require('../../css/cardForm.css');

import { 
	Card, CardImg, CardText, CardBody, 
	CardTitle, CardSubtitle, CardLink,

	Container, Row, Col,

} from 'reactstrap';

import CardImage from '../../images/delfos.png';

import LoginForm from './LoginForm';

export default class Login extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return(
			<Container>
				<Row>
					<Col sm={{ size: 6, offset: 3 }}>
						<Card className="middle" style={{ backgroundColor: 'rgba(255, 255, 255, 0.0)', borderColor: 'rgba(255, 255, 255, 0.0)' }}>
							<CardImg src={"/dist/" + CardImage} className="med-img"/>
							<CardBody>
								<CardTitle>Login</CardTitle>
								<LoginForm />

								<CardLink href="signup">Create Account</CardLink>

							</CardBody>
						</Card>
					</Col>
				</Row>
			</Container>			
		);
	}
}