import React from 'react';
var $ = require('jquery');
require('../../css/cardForm.css');

import { 

	Form, FormGroup, Label, Input, 
	FormText, Button, FormFeedback,

} from 'reactstrap';

import { connect } from "react-redux";
import { fieldChange, clearState, postToServer } from '../utils/form/formFunctions.js';
import ReduxFormFunctions from '../utils/form/reduxFormFunctions.js';

const default_state = {username: '', password: '', u_valid: null, p_valid: null};

class _LoginForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = default_state;

		this.handleFieldChange = this.handleFieldChange.bind(this);
		this.handleFormSubmit = this.handleFormSubmit.bind(this);
	}

	handleFieldChange(event) {
		this.setState(fieldChange(event));
	}

	//-------------------------------------------------------------------

	handleFormSubmit(event) {
		event.preventDefault();

		postToServer($, window, this.state)
			.done( user => { this.sessionSuccess(user); })
			.fail( (xhr, status, error) => { this.sessionFailed(xhr.responseText); });	
	}

	sessionSuccess(user) {
		clearState(this, default_state);

		this.props.StartSession(user);

		$(location).attr('href', window.location.origin + '/home');
	}

	sessionFailed(error) {
		if(error == 'username') {
			this.setState({password: ''});
			this.setState({username: ''});
			this.setState({u_valid: false});
			this.setState({p_valid: false});
		} else {
			this.setState({password: ''});
			this.setState({u_valid: true});
			this.setState({p_valid: false});
		}
	}

	//--------------------------------------------------------------------

	render() {
		return (
			<Form onSubmit={this.handleFormSubmit}>
				<FormGroup role="form">
					<Input 	name="username"
							placeholder="Username" 
							className="input"
							bsSize="lg" 
							value={this.state.username}
							onChange={this.handleFieldChange}
							valid={this.state.u_valid}/>
					<FormFeedback>Nome de usuário inexistente.</FormFeedback>

					<Input 	name="password"
							type="password" 
							placeholder="Password" 
							className="input"
							bsSize="lg" 
							value={this.state.password}
							onChange={this.handleFieldChange}
							valid={this.state.p_valid}/>
					<FormFeedback>Senha incorreta! Por favor entre uma nova senha.</FormFeedback>
				</FormGroup>
				<Button type="submit" block style={{ backgroundColor: 'rgba(255, 102, 0)', borderColor: 'rgb(255, 102, 0)' }}>Sign In</Button>
			</Form>
		);
	}
}

const LoginForm = connect(null, ReduxFormFunctions)(_LoginForm);

export default LoginForm;