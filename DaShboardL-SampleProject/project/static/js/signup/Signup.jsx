import React from 'react';
var $ = require('jquery');
require('../../css/cardForm.css');

import { 
	Card, CardImg, CardText, CardBody, 
	CardTitle, CardSubtitle, CardLink,

	Container, Row, Col,

} from 'reactstrap';

import CardImage from '../../images/delfos.png';

import SignupForm from './SignupForm';

export default class Signup extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return(
			<Container>
				<Row>
					<Col sm={{ size: 6, offset: 3 }}>
						<Card className="force-top-50" style={{ backgroundColor: 'rgba(255, 255, 255, 0.0)', borderColor: 'rgba(255, 255, 255, 0.0)' }}>
							<CardImg src={"/dist/" + CardImage} className="small-img"/>
							<CardBody>
								<CardTitle>Signup</CardTitle>
								<SignupForm />

								<CardLink href="login">Login</CardLink>

							</CardBody>
						</Card>
					</Col>
				</Row>
			</Container>			
		);
	}
}