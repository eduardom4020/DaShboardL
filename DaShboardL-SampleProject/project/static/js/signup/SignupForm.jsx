import React from 'react';
var $ = require('jquery');
require('../../css/cardForm.css');

import { 

	Form, FormGroup, Label, Input, 
	FormText, Button, FormFeedback,

} from 'reactstrap';

import { connect } from "react-redux";
import { fieldChange, clearState, postToServer } from '../utils/form/formFunctions.js';
import ReduxFormFunctions from '../utils/form/reduxFormFunctions.js';

const default_state = {username: '', password: '', fullname:'', 
						role: '', department: '', phone: '',
						confirm_password: '', u_valid: null,
						p_valid: null};

class _SignupForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = default_state;

		this.handleFieldChange = this.handleFieldChange.bind(this);
		this.handleFormSubmit = this.handleFormSubmit.bind(this);
	}

	handleFieldChange(event) {
		this.setState(fieldChange(event));
	}

	//-------------------------------------------------------------------

	handleFormSubmit(event) {
		event.preventDefault();

		postToServer($, window, this.state)
			.done( user => { this.sessionSuccess(user); })
			.fail( (xhr, status, error) => { this.sessionFailed(xhr.responseText); });	
	}

	sessionSuccess(user) {
		clearState(this, default_state);

		this.props.StartSession(user);

		$(location).attr('href', window.location.origin + '/home');
	}

	sessionFailed(error) {
		
	}

	render() {
		return (
			<Form onSubmit={this.handleFormSubmit}>
				<FormGroup>
					<Input 	placeholder="Username" 
							name="username"
							className="input"
							bsSize="md" 
							value={this.state.username}
							onChange={this.handleFieldChange}
							valid={this.state.u_valid}/>

					<FormFeedback>Gerar diferentes mensagens de erro!</FormFeedback>

					<Input 	placeholder="Full Name" 
							name="fullname"
							className="input"
							bsSize="md" 
							value={this.state.fullname}
							onChange={this.handleFieldChange}/>

					<Input 	placeholder="Role" 
							name="role"
							className="input"
							bsSize="md" 
							value={this.state.role}
							onChange={this.handleFieldChange}/>

					<Input 	placeholder="Department" 
							name="department"
							className="input"
							bsSize="md" 
							value={this.state.department}
							onChange={this.handleFieldChange}/>

					<Input 	placeholder="Phone" 
							name="phone"
							className="input"
							bsSize="md" 
							value={this.state.phone}
							onChange={this.handleFieldChange}/>

					<Input 	placeholder="Password"
							name="password"
							type="password" 
							className="input"
							bsSize="md" 
							value={this.state.password}
							onChange={this.handleFieldChange}/>

					<Input 	placeholder="Confirm Password"
							name="confirm_password"
							type="password" 
							className="input"
							bsSize="md" 
							value={this.state.confirm_password}
							onChange={this.handleFieldChange}/>

				</FormGroup>
				<Button block style={{ backgroundColor: 'rgba(255, 102, 0)', borderColor: 'rgb(255, 102, 0)' }}>Confirm</Button>
			</Form>
		);
	}
}

const SignupForm = connect(null, ReduxFormFunctions)(_SignupForm);

export default SignupForm;