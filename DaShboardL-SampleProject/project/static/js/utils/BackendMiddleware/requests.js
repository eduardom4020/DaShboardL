export function postToServer($, window, route_name, payload) {
	return $.post(window.location.origin + '/' + route_name, payload);
}