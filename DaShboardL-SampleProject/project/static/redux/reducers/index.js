import { 

	UPDATE_ROUTES, 
	
} from '../constants/action_types';

const stateHistory = [{
	last_action: 'None',
	authenticated: false,
	User: {
		name: '',
	},
	SideBar: {
		hide: true,
		options_tree: [{}],
		selected_option_path: '/',
	},
}];

var consecutive_fails_num = 0;

const rootReducer = (state = stateHistory, action) => {
	let new_state = Object.assign({}, state[state.length - 1]);
	new_state.last_action = action.type;

	// switch (action.type) {
	// 	case START_SESSION:
	// 		// If receive user data it means that this user was authenticated
	// 		if(action.user != undefined) {
	// 			new_state.authenticated = true;
	// 			new_state.User = action.user;
	// 		}

	// 		consecutive_fails_num = 0;

	// 		return [...state, new_state];

	// 	case SESSION_FAILED:
	// 		consecutive_fails_num += 1;
	// 		console.log('Consecutive Fails: ' + consecutive_fails_num);

	// 		return [...state, new_state];

	// 	case SHOW_SIDE_BAR:
	// 		new_state.SideBar.hide = false;

	// 		return [...state, new_state];

	// 	case HIDE_SIDE_BAR:
	// 		new_state.SideBar.hide = true;

	// 		return [...state, new_state];

	// 	default:
	// 		return state;
	// }
};

export default rootReducer;