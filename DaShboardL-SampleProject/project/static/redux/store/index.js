import { createStore } from "redux";
import rootReducer from "../reducers/index";

const store = createStore(rootReducer);



store.subscribe(() => {
	let prevState = currentState;
	let currentState = store.getState();

	console.log("Previous State:");
	console.log(prevState);
	console.log("Current State:");
	console.log(currentState);
	console.log("==============================================");
});

export default store;