is_virtualenv_installed=$(pip3 list | grep virtualenv);

if [ "" = "$is_virtualenv_installed" ] ; then
	echo "Installing virtualenv"
	sudo pip3 install virtualenv
fi;

if [ ! -d env/ ] ; then
	echo "Creating env"
	virtualenv -p python3 env
fi;

chmod 777 -R env/;
. env/bin/activate;

pip install --upgrade -r requirements.txt;

cd project/static/;

npm update;
npm install;

gnome-terminal --tab -e "npm run watch";
npm run server;