/**
 */
package DaShboardL.tests;

import DaShboardL.ConstantsAgregator;
import DaShboardL.DaShboardLFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Constants Agregator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConstantsAgregatorTest extends TestCase {

	/**
	 * The fixture for this Constants Agregator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstantsAgregator fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ConstantsAgregatorTest.class);
	}

	/**
	 * Constructs a new Constants Agregator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantsAgregatorTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Constants Agregator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ConstantsAgregator fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Constants Agregator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstantsAgregator getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DaShboardLFactory.eINSTANCE.createConstantsAgregator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ConstantsAgregatorTest
