/**
 */
package DaShboardL.tests;

import DaShboardL.DaShboardLFactory;
import DaShboardL.FloatActionButtom;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Float Action Buttom</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FloatActionButtomTest extends StyleTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FloatActionButtomTest.class);
	}

	/**
	 * Constructs a new Float Action Buttom test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatActionButtomTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Float Action Buttom test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FloatActionButtom getFixture() {
		return (FloatActionButtom)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DaShboardLFactory.eINSTANCE.createFloatActionButtom());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FloatActionButtomTest
