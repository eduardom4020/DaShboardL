/**
 */
package DaShboardL.tests;

import DaShboardL.VariableStyle;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Variable Style</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class VariableStyleTest extends TestCase {

	/**
	 * The fixture for this Variable Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableStyle fixture = null;

	/**
	 * Constructs a new Variable Style test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableStyleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Variable Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(VariableStyle fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Variable Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableStyle getFixture() {
		return fixture;
	}

} //VariableStyleTest
