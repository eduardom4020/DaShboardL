/**
 */
package DaShboardL.tests;

import DaShboardL.DaShboardLFactory;
import DaShboardL.xAxis;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>xAxis</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class xAxisTest extends AxisTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(xAxisTest.class);
	}

	/**
	 * Constructs a new xAxis test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public xAxisTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this xAxis test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected xAxis getFixture() {
		return (xAxis)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DaShboardLFactory.eINSTANCE.createxAxis());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //xAxisTest
