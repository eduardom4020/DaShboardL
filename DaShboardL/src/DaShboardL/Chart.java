/**
 */
package DaShboardL;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Chart</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.Chart#getTitle <em>Title</em>}</li>
 *   <li>{@link DaShboardL.Chart#getSubtitle <em>Subtitle</em>}</li>
 *   <li>{@link DaShboardL.Chart#getTooltip <em>Tooltip</em>}</li>
 *   <li>{@link DaShboardL.Chart#getChart_get_variable <em>Chart get variable</em>}</li>
 *   <li>{@link DaShboardL.Chart#getXaxis <em>Xaxis</em>}</li>
 *   <li>{@link DaShboardL.Chart#getYaxis <em>Yaxis</em>}</li>
 * </ul>
 *
 * @see DaShboardL.DaShboardLPackage#getChart()
 * @model
 * @generated
 */
public interface Chart extends Content {
	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Title</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see DaShboardL.DaShboardLPackage#getChart_Title()
	 * @model required="true"
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link DaShboardL.Chart#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Subtitle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtitle</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtitle</em>' attribute.
	 * @see #setSubtitle(String)
	 * @see DaShboardL.DaShboardLPackage#getChart_Subtitle()
	 * @model
	 * @generated
	 */
	String getSubtitle();

	/**
	 * Sets the value of the '{@link DaShboardL.Chart#getSubtitle <em>Subtitle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subtitle</em>' attribute.
	 * @see #getSubtitle()
	 * @generated
	 */
	void setSubtitle(String value);

	/**
	 * Returns the value of the '<em><b>Tooltip</b></em>' containment reference list.
	 * The list contents are of type {@link DaShboardL.Tooltip}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tooltip</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tooltip</em>' containment reference list.
	 * @see DaShboardL.DaShboardLPackage#getChart_Tooltip()
	 * @model containment="true"
	 * @generated
	 */
	EList<Tooltip> getTooltip();

	/**
	 * Returns the value of the '<em><b>Chart get variable</b></em>' reference list.
	 * The list contents are of type {@link DaShboardL.ChartVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chart get variable</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chart get variable</em>' reference list.
	 * @see DaShboardL.DaShboardLPackage#getChart_Chart_get_variable()
	 * @model
	 * @generated
	 */
	EList<ChartVariable> getChart_get_variable();

	/**
	 * Returns the value of the '<em><b>Xaxis</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xaxis</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xaxis</em>' containment reference.
	 * @see #setXaxis(xAxis)
	 * @see DaShboardL.DaShboardLPackage#getChart_Xaxis()
	 * @model containment="true"
	 * @generated
	 */
	xAxis getXaxis();

	/**
	 * Sets the value of the '{@link DaShboardL.Chart#getXaxis <em>Xaxis</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xaxis</em>' containment reference.
	 * @see #getXaxis()
	 * @generated
	 */
	void setXaxis(xAxis value);

	/**
	 * Returns the value of the '<em><b>Yaxis</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Yaxis</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Yaxis</em>' containment reference.
	 * @see #setYaxis(yAxis)
	 * @see DaShboardL.DaShboardLPackage#getChart_Yaxis()
	 * @model containment="true"
	 * @generated
	 */
	yAxis getYaxis();

	/**
	 * Sets the value of the '{@link DaShboardL.Chart#getYaxis <em>Yaxis</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Yaxis</em>' containment reference.
	 * @see #getYaxis()
	 * @generated
	 */
	void setYaxis(yAxis value);

} // Chart
