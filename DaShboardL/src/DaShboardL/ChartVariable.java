/**
 */
package DaShboardL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Chart Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.ChartVariable#getId <em>Id</em>}</li>
 *   <li>{@link DaShboardL.ChartVariable#getName <em>Name</em>}</li>
 *   <li>{@link DaShboardL.ChartVariable#getMeasure <em>Measure</em>}</li>
 *   <li>{@link DaShboardL.ChartVariable#getVariablestyle <em>Variablestyle</em>}</li>
 * </ul>
 *
 * @see DaShboardL.DaShboardLPackage#getChartVariable()
 * @model
 * @generated
 */
public interface ChartVariable extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see DaShboardL.DaShboardLPackage#getChartVariable_Id()
	 * @model required="true"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link DaShboardL.ChartVariable#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see DaShboardL.DaShboardLPackage#getChartVariable_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link DaShboardL.ChartVariable#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measure</em>' attribute.
	 * @see #setMeasure(String)
	 * @see DaShboardL.DaShboardLPackage#getChartVariable_Measure()
	 * @model
	 * @generated
	 */
	String getMeasure();

	/**
	 * Sets the value of the '{@link DaShboardL.ChartVariable#getMeasure <em>Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Measure</em>' attribute.
	 * @see #getMeasure()
	 * @generated
	 */
	void setMeasure(String value);

	/**
	 * Returns the value of the '<em><b>Variablestyle</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variablestyle</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variablestyle</em>' containment reference.
	 * @see #setVariablestyle(VariableStyle)
	 * @see DaShboardL.DaShboardLPackage#getChartVariable_Variablestyle()
	 * @model containment="true"
	 * @generated
	 */
	VariableStyle getVariablestyle();

	/**
	 * Sets the value of the '{@link DaShboardL.ChartVariable#getVariablestyle <em>Variablestyle</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variablestyle</em>' containment reference.
	 * @see #getVariablestyle()
	 * @generated
	 */
	void setVariablestyle(VariableStyle value);

} // ChartVariable
