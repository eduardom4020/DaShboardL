/**
 */
package DaShboardL;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constants Agregator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.ConstantsAgregator#getFunction <em>Function</em>}</li>
 *   <li>{@link DaShboardL.ConstantsAgregator#getScale <em>Scale</em>}</li>
 *   <li>{@link DaShboardL.ConstantsAgregator#getPosition <em>Position</em>}</li>
 *   <li>{@link DaShboardL.ConstantsAgregator#getChartvariable <em>Chartvariable</em>}</li>
 * </ul>
 *
 * @see DaShboardL.DaShboardLPackage#getConstantsAgregator()
 * @model
 * @generated
 */
public interface ConstantsAgregator extends EObject {
	/**
	 * Returns the value of the '<em><b>Function</b></em>' containment reference list.
	 * The list contents are of type {@link DaShboardL.Function}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function</em>' containment reference list.
	 * @see DaShboardL.DaShboardLPackage#getConstantsAgregator_Function()
	 * @model containment="true"
	 * @generated
	 */
	EList<Function> getFunction();

	/**
	 * Returns the value of the '<em><b>Scale</b></em>' containment reference list.
	 * The list contents are of type {@link DaShboardL.Scale}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale</em>' containment reference list.
	 * @see DaShboardL.DaShboardLPackage#getConstantsAgregator_Scale()
	 * @model containment="true"
	 * @generated
	 */
	EList<Scale> getScale();

	/**
	 * Returns the value of the '<em><b>Position</b></em>' containment reference list.
	 * The list contents are of type {@link DaShboardL.Position}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' containment reference list.
	 * @see DaShboardL.DaShboardLPackage#getConstantsAgregator_Position()
	 * @model containment="true"
	 * @generated
	 */
	EList<Position> getPosition();

	/**
	 * Returns the value of the '<em><b>Chartvariable</b></em>' containment reference list.
	 * The list contents are of type {@link DaShboardL.ChartVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chartvariable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chartvariable</em>' containment reference list.
	 * @see DaShboardL.DaShboardLPackage#getConstantsAgregator_Chartvariable()
	 * @model containment="true"
	 * @generated
	 */
	EList<ChartVariable> getChartvariable();

} // ConstantsAgregator
