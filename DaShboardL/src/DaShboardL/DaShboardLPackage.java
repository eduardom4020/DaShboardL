/**
 */
package DaShboardL;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see DaShboardL.DaShboardLFactory
 * @model kind="package"
 * @generated
 */
public interface DaShboardLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "DaShboardL";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/obeo/dashboardl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "DaShboardL";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DaShboardLPackage eINSTANCE = DaShboardL.impl.DaShboardLPackageImpl.init();

	/**
	 * The meta object id for the '{@link DaShboardL.impl.DashboardImpl <em>Dashboard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.DashboardImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getDashboard()
	 * @generated
	 */
	int DASHBOARD = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD__ID = 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD__VERSION = 1;

	/**
	 * The feature id for the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD__PORT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD__NAME = 3;

	/**
	 * The feature id for the '<em><b>Page</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD__PAGE = 4;

	/**
	 * The number of structural features of the '<em>Dashboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Dashboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.ContainerImpl <em>Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.ContainerImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getContainer()
	 * @generated
	 */
	int CONTAINER = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__ID = 0;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__LABEL = 1;

	/**
	 * The feature id for the '<em><b>Label hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__LABEL_HIDDEN = 2;

	/**
	 * The feature id for the '<em><b>Position</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__POSITION = 3;

	/**
	 * The feature id for the '<em><b>Scale</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__SCALE = 4;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__CONTENT = 5;

	/**
	 * The number of structural features of the '<em>Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.ContentImpl <em>Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.ContentImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getContent()
	 * @generated
	 */
	int CONTENT = 21;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT__ID = 0;

	/**
	 * The number of structural features of the '<em>Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.MenuImpl <em>Menu</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.MenuImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getMenu()
	 * @generated
	 */
	int MENU = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MENU__ID = CONTENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MENU__NAME = CONTENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Style</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MENU__STYLE = CONTENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Menu get function</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MENU__MENU_GET_FUNCTION = CONTENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Menu</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MENU_FEATURE_COUNT = CONTENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Menu</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MENU_OPERATION_COUNT = CONTENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.ChartImpl <em>Chart</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.ChartImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getChart()
	 * @generated
	 */
	int CHART = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART__ID = CONTENT__ID;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART__TITLE = CONTENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Subtitle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART__SUBTITLE = CONTENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tooltip</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART__TOOLTIP = CONTENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Chart get variable</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART__CHART_GET_VARIABLE = CONTENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Xaxis</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART__XAXIS = CONTENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Yaxis</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART__YAXIS = CONTENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Chart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_FEATURE_COUNT = CONTENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Chart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_OPERATION_COUNT = CONTENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.TooltipImpl <em>Tooltip</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.TooltipImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getTooltip()
	 * @generated
	 */
	int TOOLTIP = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOLTIP__ID = 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOLTIP__TEXT = 1;

	/**
	 * The feature id for the '<em><b>Chartvariable</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOLTIP__CHARTVARIABLE = 2;

	/**
	 * The number of structural features of the '<em>Tooltip</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOLTIP_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Tooltip</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOOLTIP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.StyleImpl <em>Style</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.StyleImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getStyle()
	 * @generated
	 */
	int STYLE = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STYLE__ID = 0;

	/**
	 * The number of structural features of the '<em>Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STYLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STYLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.FloatActionButtomImpl <em>Float Action Buttom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.FloatActionButtomImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getFloatActionButtom()
	 * @generated
	 */
	int FLOAT_ACTION_BUTTOM = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_ACTION_BUTTOM__ID = STYLE__ID;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_ACTION_BUTTOM__ICON = STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Float Action Buttom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_ACTION_BUTTOM_FEATURE_COUNT = STYLE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Float Action Buttom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_ACTION_BUTTOM_OPERATION_COUNT = STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.ListImpl <em>List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.ListImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getList()
	 * @generated
	 */
	int LIST = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST__ID = STYLE__ID;

	/**
	 * The feature id for the '<em><b>Orientation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST__ORIENTATION = STYLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Appear</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST__APPEAR = STYLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_FEATURE_COUNT = STYLE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_OPERATION_COUNT = STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.RoundImpl <em>Round</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.RoundImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getRound()
	 * @generated
	 */
	int ROUND = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUND__ID = STYLE__ID;

	/**
	 * The number of structural features of the '<em>Round</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUND_FEATURE_COUNT = STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Round</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUND_OPERATION_COUNT = STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.DropdownImpl <em>Dropdown</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.DropdownImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getDropdown()
	 * @generated
	 */
	int DROPDOWN = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DROPDOWN__ID = STYLE__ID;

	/**
	 * The number of structural features of the '<em>Dropdown</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DROPDOWN_FEATURE_COUNT = STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dropdown</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DROPDOWN_OPERATION_COUNT = STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.FunctionImpl <em>Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.FunctionImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getFunction()
	 * @generated
	 */
	int FUNCTION = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__ID = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__CODE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__NAME = 2;

	/**
	 * The feature id for the '<em><b>Page</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__PAGE = 3;

	/**
	 * The number of structural features of the '<em>Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.ScaleImpl <em>Scale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.ScaleImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getScale()
	 * @generated
	 */
	int SCALE = 11;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE__ID = 0;

	/**
	 * The feature id for the '<em><b>Axis</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE__AXIS = 1;

	/**
	 * The feature id for the '<em><b>Times</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE__TIMES = 2;

	/**
	 * The number of structural features of the '<em>Scale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Scale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.ChartVariableImpl <em>Chart Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.ChartVariableImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getChartVariable()
	 * @generated
	 */
	int CHART_VARIABLE = 12;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VARIABLE__ID = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VARIABLE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VARIABLE__MEASURE = 2;

	/**
	 * The feature id for the '<em><b>Variablestyle</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VARIABLE__VARIABLESTYLE = 3;

	/**
	 * The number of structural features of the '<em>Chart Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VARIABLE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Chart Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VARIABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.PositionImpl <em>Position</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.PositionImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getPosition()
	 * @generated
	 */
	int POSITION = 13;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__ID = 0;

	/**
	 * The feature id for the '<em><b>Axis</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__AXIS = 1;

	/**
	 * The feature id for the '<em><b>Distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__DISTANCE = 2;

	/**
	 * The number of structural features of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.ConstantsAgregatorImpl <em>Constants Agregator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.ConstantsAgregatorImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getConstantsAgregator()
	 * @generated
	 */
	int CONSTANTS_AGREGATOR = 14;

	/**
	 * The feature id for the '<em><b>Function</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANTS_AGREGATOR__FUNCTION = 0;

	/**
	 * The feature id for the '<em><b>Scale</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANTS_AGREGATOR__SCALE = 1;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANTS_AGREGATOR__POSITION = 2;

	/**
	 * The feature id for the '<em><b>Chartvariable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANTS_AGREGATOR__CHARTVARIABLE = 3;

	/**
	 * The number of structural features of the '<em>Constants Agregator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANTS_AGREGATOR_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Constants Agregator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANTS_AGREGATOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.ProjectImpl <em>Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.ProjectImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getProject()
	 * @generated
	 */
	int PROJECT = 15;

	/**
	 * The feature id for the '<em><b>Dashboard</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__DASHBOARD = 0;

	/**
	 * The feature id for the '<em><b>Constantsagregator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__CONSTANTSAGREGATOR = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__ID = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__NAME = 3;

	/**
	 * The number of structural features of the '<em>Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.VariableStyleImpl <em>Variable Style</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.VariableStyleImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getVariableStyle()
	 * @generated
	 */
	int VARIABLE_STYLE = 16;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STYLE__COLOR = 0;

	/**
	 * The number of structural features of the '<em>Variable Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STYLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Variable Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STYLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.BarImpl <em>Bar</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.BarImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getBar()
	 * @generated
	 */
	int BAR = 17;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAR__COLOR = VARIABLE_STYLE__COLOR;

	/**
	 * The feature id for the '<em><b>Orientation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAR__ORIENTATION = VARIABLE_STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Bar</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAR_FEATURE_COUNT = VARIABLE_STYLE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Bar</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAR_OPERATION_COUNT = VARIABLE_STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.LineImpl <em>Line</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.LineImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getLine()
	 * @generated
	 */
	int LINE = 18;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__COLOR = VARIABLE_STYLE__COLOR;

	/**
	 * The feature id for the '<em><b>Fill</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__FILL = VARIABLE_STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Line</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE_FEATURE_COUNT = VARIABLE_STYLE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Line</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE_OPERATION_COUNT = VARIABLE_STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.PointImpl <em>Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.PointImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getPoint()
	 * @generated
	 */
	int POINT = 19;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT__COLOR = VARIABLE_STYLE__COLOR;

	/**
	 * The number of structural features of the '<em>Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_FEATURE_COUNT = VARIABLE_STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_OPERATION_COUNT = VARIABLE_STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.PageImpl <em>Page</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.PageImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getPage()
	 * @generated
	 */
	int PAGE = 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Container</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE__CONTAINER = 1;

	/**
	 * The number of structural features of the '<em>Page</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Page</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.AxisImpl <em>Axis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.AxisImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getAxis()
	 * @generated
	 */
	int AXIS = 22;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__MIN = 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__MAX = 1;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__TITLE = 2;

	/**
	 * The feature id for the '<em><b>Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__LABELS = 3;

	/**
	 * The number of structural features of the '<em>Axis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Axis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link DaShboardL.impl.xAxisImpl <em>xAxis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.xAxisImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getxAxis()
	 * @generated
	 */
	int XAXIS = 23;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XAXIS__MIN = AXIS__MIN;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XAXIS__MAX = AXIS__MAX;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XAXIS__TITLE = AXIS__TITLE;

	/**
	 * The feature id for the '<em><b>Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XAXIS__LABELS = AXIS__LABELS;

	/**
	 * The number of structural features of the '<em>xAxis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XAXIS_FEATURE_COUNT = AXIS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>xAxis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XAXIS_OPERATION_COUNT = AXIS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DaShboardL.impl.yAxisImpl <em>yAxis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DaShboardL.impl.yAxisImpl
	 * @see DaShboardL.impl.DaShboardLPackageImpl#getyAxis()
	 * @generated
	 */
	int YAXIS = 24;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAXIS__MIN = AXIS__MIN;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAXIS__MAX = AXIS__MAX;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAXIS__TITLE = AXIS__TITLE;

	/**
	 * The feature id for the '<em><b>Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAXIS__LABELS = AXIS__LABELS;

	/**
	 * The number of structural features of the '<em>yAxis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAXIS_FEATURE_COUNT = AXIS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>yAxis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAXIS_OPERATION_COUNT = AXIS_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link DaShboardL.Dashboard <em>Dashboard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dashboard</em>'.
	 * @see DaShboardL.Dashboard
	 * @generated
	 */
	EClass getDashboard();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Dashboard#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see DaShboardL.Dashboard#getId()
	 * @see #getDashboard()
	 * @generated
	 */
	EAttribute getDashboard_Id();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Dashboard#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see DaShboardL.Dashboard#getVersion()
	 * @see #getDashboard()
	 * @generated
	 */
	EAttribute getDashboard_Version();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Dashboard#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port</em>'.
	 * @see DaShboardL.Dashboard#getPort()
	 * @see #getDashboard()
	 * @generated
	 */
	EAttribute getDashboard_Port();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Dashboard#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DaShboardL.Dashboard#getName()
	 * @see #getDashboard()
	 * @generated
	 */
	EAttribute getDashboard_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link DaShboardL.Dashboard#getPage <em>Page</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Page</em>'.
	 * @see DaShboardL.Dashboard#getPage()
	 * @see #getDashboard()
	 * @generated
	 */
	EReference getDashboard_Page();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Container <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Container</em>'.
	 * @see DaShboardL.Container
	 * @generated
	 */
	EClass getContainer();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Container#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see DaShboardL.Container#getId()
	 * @see #getContainer()
	 * @generated
	 */
	EAttribute getContainer_Id();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Container#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see DaShboardL.Container#getLabel()
	 * @see #getContainer()
	 * @generated
	 */
	EAttribute getContainer_Label();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Container#isLabel_hidden <em>Label hidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label hidden</em>'.
	 * @see DaShboardL.Container#isLabel_hidden()
	 * @see #getContainer()
	 * @generated
	 */
	EAttribute getContainer_Label_hidden();

	/**
	 * Returns the meta object for the reference list '{@link DaShboardL.Container#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Position</em>'.
	 * @see DaShboardL.Container#getPosition()
	 * @see #getContainer()
	 * @generated
	 */
	EReference getContainer_Position();

	/**
	 * Returns the meta object for the reference list '{@link DaShboardL.Container#getScale <em>Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Scale</em>'.
	 * @see DaShboardL.Container#getScale()
	 * @see #getContainer()
	 * @generated
	 */
	EReference getContainer_Scale();

	/**
	 * Returns the meta object for the containment reference '{@link DaShboardL.Container#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Content</em>'.
	 * @see DaShboardL.Container#getContent()
	 * @see #getContainer()
	 * @generated
	 */
	EReference getContainer_Content();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Menu <em>Menu</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Menu</em>'.
	 * @see DaShboardL.Menu
	 * @generated
	 */
	EClass getMenu();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Menu#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DaShboardL.Menu#getName()
	 * @see #getMenu()
	 * @generated
	 */
	EAttribute getMenu_Name();

	/**
	 * Returns the meta object for the containment reference '{@link DaShboardL.Menu#getStyle <em>Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Style</em>'.
	 * @see DaShboardL.Menu#getStyle()
	 * @see #getMenu()
	 * @generated
	 */
	EReference getMenu_Style();

	/**
	 * Returns the meta object for the reference list '{@link DaShboardL.Menu#getMenu_get_function <em>Menu get function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Menu get function</em>'.
	 * @see DaShboardL.Menu#getMenu_get_function()
	 * @see #getMenu()
	 * @generated
	 */
	EReference getMenu_Menu_get_function();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Chart <em>Chart</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Chart</em>'.
	 * @see DaShboardL.Chart
	 * @generated
	 */
	EClass getChart();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Chart#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see DaShboardL.Chart#getTitle()
	 * @see #getChart()
	 * @generated
	 */
	EAttribute getChart_Title();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Chart#getSubtitle <em>Subtitle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Subtitle</em>'.
	 * @see DaShboardL.Chart#getSubtitle()
	 * @see #getChart()
	 * @generated
	 */
	EAttribute getChart_Subtitle();

	/**
	 * Returns the meta object for the containment reference list '{@link DaShboardL.Chart#getTooltip <em>Tooltip</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tooltip</em>'.
	 * @see DaShboardL.Chart#getTooltip()
	 * @see #getChart()
	 * @generated
	 */
	EReference getChart_Tooltip();

	/**
	 * Returns the meta object for the reference list '{@link DaShboardL.Chart#getChart_get_variable <em>Chart get variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Chart get variable</em>'.
	 * @see DaShboardL.Chart#getChart_get_variable()
	 * @see #getChart()
	 * @generated
	 */
	EReference getChart_Chart_get_variable();

	/**
	 * Returns the meta object for the containment reference '{@link DaShboardL.Chart#getXaxis <em>Xaxis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Xaxis</em>'.
	 * @see DaShboardL.Chart#getXaxis()
	 * @see #getChart()
	 * @generated
	 */
	EReference getChart_Xaxis();

	/**
	 * Returns the meta object for the containment reference '{@link DaShboardL.Chart#getYaxis <em>Yaxis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Yaxis</em>'.
	 * @see DaShboardL.Chart#getYaxis()
	 * @see #getChart()
	 * @generated
	 */
	EReference getChart_Yaxis();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Tooltip <em>Tooltip</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tooltip</em>'.
	 * @see DaShboardL.Tooltip
	 * @generated
	 */
	EClass getTooltip();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Tooltip#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see DaShboardL.Tooltip#getId()
	 * @see #getTooltip()
	 * @generated
	 */
	EAttribute getTooltip_Id();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Tooltip#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see DaShboardL.Tooltip#getText()
	 * @see #getTooltip()
	 * @generated
	 */
	EAttribute getTooltip_Text();

	/**
	 * Returns the meta object for the reference list '{@link DaShboardL.Tooltip#getChartvariable <em>Chartvariable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Chartvariable</em>'.
	 * @see DaShboardL.Tooltip#getChartvariable()
	 * @see #getTooltip()
	 * @generated
	 */
	EReference getTooltip_Chartvariable();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Style <em>Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Style</em>'.
	 * @see DaShboardL.Style
	 * @generated
	 */
	EClass getStyle();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Style#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see DaShboardL.Style#getId()
	 * @see #getStyle()
	 * @generated
	 */
	EAttribute getStyle_Id();

	/**
	 * Returns the meta object for class '{@link DaShboardL.FloatActionButtom <em>Float Action Buttom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Action Buttom</em>'.
	 * @see DaShboardL.FloatActionButtom
	 * @generated
	 */
	EClass getFloatActionButtom();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.FloatActionButtom#getIcon <em>Icon</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Icon</em>'.
	 * @see DaShboardL.FloatActionButtom#getIcon()
	 * @see #getFloatActionButtom()
	 * @generated
	 */
	EAttribute getFloatActionButtom_Icon();

	/**
	 * Returns the meta object for class '{@link DaShboardL.List <em>List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List</em>'.
	 * @see DaShboardL.List
	 * @generated
	 */
	EClass getList();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.List#getOrientation <em>Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Orientation</em>'.
	 * @see DaShboardL.List#getOrientation()
	 * @see #getList()
	 * @generated
	 */
	EAttribute getList_Orientation();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.List#isAppear <em>Appear</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Appear</em>'.
	 * @see DaShboardL.List#isAppear()
	 * @see #getList()
	 * @generated
	 */
	EAttribute getList_Appear();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Round <em>Round</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Round</em>'.
	 * @see DaShboardL.Round
	 * @generated
	 */
	EClass getRound();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Dropdown <em>Dropdown</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dropdown</em>'.
	 * @see DaShboardL.Dropdown
	 * @generated
	 */
	EClass getDropdown();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Function <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function</em>'.
	 * @see DaShboardL.Function
	 * @generated
	 */
	EClass getFunction();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Function#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see DaShboardL.Function#getId()
	 * @see #getFunction()
	 * @generated
	 */
	EAttribute getFunction_Id();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Function#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see DaShboardL.Function#getCode()
	 * @see #getFunction()
	 * @generated
	 */
	EAttribute getFunction_Code();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Function#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DaShboardL.Function#getName()
	 * @see #getFunction()
	 * @generated
	 */
	EAttribute getFunction_Name();

	/**
	 * Returns the meta object for the reference '{@link DaShboardL.Function#getPage <em>Page</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Page</em>'.
	 * @see DaShboardL.Function#getPage()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_Page();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Scale <em>Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scale</em>'.
	 * @see DaShboardL.Scale
	 * @generated
	 */
	EClass getScale();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Scale#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see DaShboardL.Scale#getId()
	 * @see #getScale()
	 * @generated
	 */
	EAttribute getScale_Id();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Scale#getAxis <em>Axis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Axis</em>'.
	 * @see DaShboardL.Scale#getAxis()
	 * @see #getScale()
	 * @generated
	 */
	EAttribute getScale_Axis();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Scale#getTimes <em>Times</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Times</em>'.
	 * @see DaShboardL.Scale#getTimes()
	 * @see #getScale()
	 * @generated
	 */
	EAttribute getScale_Times();

	/**
	 * Returns the meta object for class '{@link DaShboardL.ChartVariable <em>Chart Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Chart Variable</em>'.
	 * @see DaShboardL.ChartVariable
	 * @generated
	 */
	EClass getChartVariable();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.ChartVariable#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see DaShboardL.ChartVariable#getId()
	 * @see #getChartVariable()
	 * @generated
	 */
	EAttribute getChartVariable_Id();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.ChartVariable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DaShboardL.ChartVariable#getName()
	 * @see #getChartVariable()
	 * @generated
	 */
	EAttribute getChartVariable_Name();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.ChartVariable#getMeasure <em>Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Measure</em>'.
	 * @see DaShboardL.ChartVariable#getMeasure()
	 * @see #getChartVariable()
	 * @generated
	 */
	EAttribute getChartVariable_Measure();

	/**
	 * Returns the meta object for the containment reference '{@link DaShboardL.ChartVariable#getVariablestyle <em>Variablestyle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Variablestyle</em>'.
	 * @see DaShboardL.ChartVariable#getVariablestyle()
	 * @see #getChartVariable()
	 * @generated
	 */
	EReference getChartVariable_Variablestyle();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Position <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Position</em>'.
	 * @see DaShboardL.Position
	 * @generated
	 */
	EClass getPosition();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Position#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see DaShboardL.Position#getId()
	 * @see #getPosition()
	 * @generated
	 */
	EAttribute getPosition_Id();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Position#getAxis <em>Axis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Axis</em>'.
	 * @see DaShboardL.Position#getAxis()
	 * @see #getPosition()
	 * @generated
	 */
	EAttribute getPosition_Axis();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Position#getDistance <em>Distance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distance</em>'.
	 * @see DaShboardL.Position#getDistance()
	 * @see #getPosition()
	 * @generated
	 */
	EAttribute getPosition_Distance();

	/**
	 * Returns the meta object for class '{@link DaShboardL.ConstantsAgregator <em>Constants Agregator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constants Agregator</em>'.
	 * @see DaShboardL.ConstantsAgregator
	 * @generated
	 */
	EClass getConstantsAgregator();

	/**
	 * Returns the meta object for the containment reference list '{@link DaShboardL.ConstantsAgregator#getFunction <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Function</em>'.
	 * @see DaShboardL.ConstantsAgregator#getFunction()
	 * @see #getConstantsAgregator()
	 * @generated
	 */
	EReference getConstantsAgregator_Function();

	/**
	 * Returns the meta object for the containment reference list '{@link DaShboardL.ConstantsAgregator#getScale <em>Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Scale</em>'.
	 * @see DaShboardL.ConstantsAgregator#getScale()
	 * @see #getConstantsAgregator()
	 * @generated
	 */
	EReference getConstantsAgregator_Scale();

	/**
	 * Returns the meta object for the containment reference list '{@link DaShboardL.ConstantsAgregator#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Position</em>'.
	 * @see DaShboardL.ConstantsAgregator#getPosition()
	 * @see #getConstantsAgregator()
	 * @generated
	 */
	EReference getConstantsAgregator_Position();

	/**
	 * Returns the meta object for the containment reference list '{@link DaShboardL.ConstantsAgregator#getChartvariable <em>Chartvariable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Chartvariable</em>'.
	 * @see DaShboardL.ConstantsAgregator#getChartvariable()
	 * @see #getConstantsAgregator()
	 * @generated
	 */
	EReference getConstantsAgregator_Chartvariable();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Project <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Project</em>'.
	 * @see DaShboardL.Project
	 * @generated
	 */
	EClass getProject();

	/**
	 * Returns the meta object for the containment reference list '{@link DaShboardL.Project#getDashboard <em>Dashboard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dashboard</em>'.
	 * @see DaShboardL.Project#getDashboard()
	 * @see #getProject()
	 * @generated
	 */
	EReference getProject_Dashboard();

	/**
	 * Returns the meta object for the containment reference '{@link DaShboardL.Project#getConstantsagregator <em>Constantsagregator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constantsagregator</em>'.
	 * @see DaShboardL.Project#getConstantsagregator()
	 * @see #getProject()
	 * @generated
	 */
	EReference getProject_Constantsagregator();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Project#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see DaShboardL.Project#getId()
	 * @see #getProject()
	 * @generated
	 */
	EAttribute getProject_Id();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Project#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DaShboardL.Project#getName()
	 * @see #getProject()
	 * @generated
	 */
	EAttribute getProject_Name();

	/**
	 * Returns the meta object for class '{@link DaShboardL.VariableStyle <em>Variable Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Style</em>'.
	 * @see DaShboardL.VariableStyle
	 * @generated
	 */
	EClass getVariableStyle();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.VariableStyle#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see DaShboardL.VariableStyle#getColor()
	 * @see #getVariableStyle()
	 * @generated
	 */
	EAttribute getVariableStyle_Color();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Bar <em>Bar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bar</em>'.
	 * @see DaShboardL.Bar
	 * @generated
	 */
	EClass getBar();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Bar#getOrientation <em>Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Orientation</em>'.
	 * @see DaShboardL.Bar#getOrientation()
	 * @see #getBar()
	 * @generated
	 */
	EAttribute getBar_Orientation();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Line <em>Line</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Line</em>'.
	 * @see DaShboardL.Line
	 * @generated
	 */
	EClass getLine();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Line#isFill <em>Fill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fill</em>'.
	 * @see DaShboardL.Line#isFill()
	 * @see #getLine()
	 * @generated
	 */
	EAttribute getLine_Fill();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Point <em>Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point</em>'.
	 * @see DaShboardL.Point
	 * @generated
	 */
	EClass getPoint();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Page <em>Page</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Page</em>'.
	 * @see DaShboardL.Page
	 * @generated
	 */
	EClass getPage();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Page#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DaShboardL.Page#getName()
	 * @see #getPage()
	 * @generated
	 */
	EAttribute getPage_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link DaShboardL.Page#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Container</em>'.
	 * @see DaShboardL.Page#getContainer()
	 * @see #getPage()
	 * @generated
	 */
	EReference getPage_Container();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Content <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Content</em>'.
	 * @see DaShboardL.Content
	 * @generated
	 */
	EClass getContent();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Content#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see DaShboardL.Content#getId()
	 * @see #getContent()
	 * @generated
	 */
	EAttribute getContent_Id();

	/**
	 * Returns the meta object for class '{@link DaShboardL.Axis <em>Axis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Axis</em>'.
	 * @see DaShboardL.Axis
	 * @generated
	 */
	EClass getAxis();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Axis#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see DaShboardL.Axis#getMin()
	 * @see #getAxis()
	 * @generated
	 */
	EAttribute getAxis_Min();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Axis#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see DaShboardL.Axis#getMax()
	 * @see #getAxis()
	 * @generated
	 */
	EAttribute getAxis_Max();

	/**
	 * Returns the meta object for the attribute '{@link DaShboardL.Axis#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see DaShboardL.Axis#getTitle()
	 * @see #getAxis()
	 * @generated
	 */
	EAttribute getAxis_Title();

	/**
	 * Returns the meta object for the attribute list '{@link DaShboardL.Axis#getLabels <em>Labels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Labels</em>'.
	 * @see DaShboardL.Axis#getLabels()
	 * @see #getAxis()
	 * @generated
	 */
	EAttribute getAxis_Labels();

	/**
	 * Returns the meta object for class '{@link DaShboardL.xAxis <em>xAxis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>xAxis</em>'.
	 * @see DaShboardL.xAxis
	 * @generated
	 */
	EClass getxAxis();

	/**
	 * Returns the meta object for class '{@link DaShboardL.yAxis <em>yAxis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>yAxis</em>'.
	 * @see DaShboardL.yAxis
	 * @generated
	 */
	EClass getyAxis();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DaShboardLFactory getDaShboardLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link DaShboardL.impl.DashboardImpl <em>Dashboard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.DashboardImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getDashboard()
		 * @generated
		 */
		EClass DASHBOARD = eINSTANCE.getDashboard();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DASHBOARD__ID = eINSTANCE.getDashboard_Id();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DASHBOARD__VERSION = eINSTANCE.getDashboard_Version();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DASHBOARD__PORT = eINSTANCE.getDashboard_Port();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DASHBOARD__NAME = eINSTANCE.getDashboard_Name();

		/**
		 * The meta object literal for the '<em><b>Page</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DASHBOARD__PAGE = eINSTANCE.getDashboard_Page();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.ContainerImpl <em>Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.ContainerImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getContainer()
		 * @generated
		 */
		EClass CONTAINER = eINSTANCE.getContainer();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTAINER__ID = eINSTANCE.getContainer_Id();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTAINER__LABEL = eINSTANCE.getContainer_Label();

		/**
		 * The meta object literal for the '<em><b>Label hidden</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTAINER__LABEL_HIDDEN = eINSTANCE.getContainer_Label_hidden();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER__POSITION = eINSTANCE.getContainer_Position();

		/**
		 * The meta object literal for the '<em><b>Scale</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER__SCALE = eINSTANCE.getContainer_Scale();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER__CONTENT = eINSTANCE.getContainer_Content();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.MenuImpl <em>Menu</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.MenuImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getMenu()
		 * @generated
		 */
		EClass MENU = eINSTANCE.getMenu();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MENU__NAME = eINSTANCE.getMenu_Name();

		/**
		 * The meta object literal for the '<em><b>Style</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MENU__STYLE = eINSTANCE.getMenu_Style();

		/**
		 * The meta object literal for the '<em><b>Menu get function</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MENU__MENU_GET_FUNCTION = eINSTANCE.getMenu_Menu_get_function();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.ChartImpl <em>Chart</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.ChartImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getChart()
		 * @generated
		 */
		EClass CHART = eINSTANCE.getChart();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART__TITLE = eINSTANCE.getChart_Title();

		/**
		 * The meta object literal for the '<em><b>Subtitle</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART__SUBTITLE = eINSTANCE.getChart_Subtitle();

		/**
		 * The meta object literal for the '<em><b>Tooltip</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART__TOOLTIP = eINSTANCE.getChart_Tooltip();

		/**
		 * The meta object literal for the '<em><b>Chart get variable</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART__CHART_GET_VARIABLE = eINSTANCE.getChart_Chart_get_variable();

		/**
		 * The meta object literal for the '<em><b>Xaxis</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART__XAXIS = eINSTANCE.getChart_Xaxis();

		/**
		 * The meta object literal for the '<em><b>Yaxis</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART__YAXIS = eINSTANCE.getChart_Yaxis();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.TooltipImpl <em>Tooltip</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.TooltipImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getTooltip()
		 * @generated
		 */
		EClass TOOLTIP = eINSTANCE.getTooltip();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOOLTIP__ID = eINSTANCE.getTooltip_Id();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOOLTIP__TEXT = eINSTANCE.getTooltip_Text();

		/**
		 * The meta object literal for the '<em><b>Chartvariable</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOOLTIP__CHARTVARIABLE = eINSTANCE.getTooltip_Chartvariable();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.StyleImpl <em>Style</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.StyleImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getStyle()
		 * @generated
		 */
		EClass STYLE = eINSTANCE.getStyle();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STYLE__ID = eINSTANCE.getStyle_Id();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.FloatActionButtomImpl <em>Float Action Buttom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.FloatActionButtomImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getFloatActionButtom()
		 * @generated
		 */
		EClass FLOAT_ACTION_BUTTOM = eINSTANCE.getFloatActionButtom();

		/**
		 * The meta object literal for the '<em><b>Icon</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_ACTION_BUTTOM__ICON = eINSTANCE.getFloatActionButtom_Icon();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.ListImpl <em>List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.ListImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getList()
		 * @generated
		 */
		EClass LIST = eINSTANCE.getList();

		/**
		 * The meta object literal for the '<em><b>Orientation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIST__ORIENTATION = eINSTANCE.getList_Orientation();

		/**
		 * The meta object literal for the '<em><b>Appear</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIST__APPEAR = eINSTANCE.getList_Appear();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.RoundImpl <em>Round</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.RoundImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getRound()
		 * @generated
		 */
		EClass ROUND = eINSTANCE.getRound();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.DropdownImpl <em>Dropdown</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.DropdownImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getDropdown()
		 * @generated
		 */
		EClass DROPDOWN = eINSTANCE.getDropdown();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.FunctionImpl <em>Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.FunctionImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getFunction()
		 * @generated
		 */
		EClass FUNCTION = eINSTANCE.getFunction();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION__ID = eINSTANCE.getFunction_Id();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION__CODE = eINSTANCE.getFunction_Code();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION__NAME = eINSTANCE.getFunction_Name();

		/**
		 * The meta object literal for the '<em><b>Page</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION__PAGE = eINSTANCE.getFunction_Page();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.ScaleImpl <em>Scale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.ScaleImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getScale()
		 * @generated
		 */
		EClass SCALE = eINSTANCE.getScale();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCALE__ID = eINSTANCE.getScale_Id();

		/**
		 * The meta object literal for the '<em><b>Axis</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCALE__AXIS = eINSTANCE.getScale_Axis();

		/**
		 * The meta object literal for the '<em><b>Times</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCALE__TIMES = eINSTANCE.getScale_Times();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.ChartVariableImpl <em>Chart Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.ChartVariableImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getChartVariable()
		 * @generated
		 */
		EClass CHART_VARIABLE = eINSTANCE.getChartVariable();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART_VARIABLE__ID = eINSTANCE.getChartVariable_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART_VARIABLE__NAME = eINSTANCE.getChartVariable_Name();

		/**
		 * The meta object literal for the '<em><b>Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART_VARIABLE__MEASURE = eINSTANCE.getChartVariable_Measure();

		/**
		 * The meta object literal for the '<em><b>Variablestyle</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_VARIABLE__VARIABLESTYLE = eINSTANCE.getChartVariable_Variablestyle();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.PositionImpl <em>Position</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.PositionImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getPosition()
		 * @generated
		 */
		EClass POSITION = eINSTANCE.getPosition();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POSITION__ID = eINSTANCE.getPosition_Id();

		/**
		 * The meta object literal for the '<em><b>Axis</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POSITION__AXIS = eINSTANCE.getPosition_Axis();

		/**
		 * The meta object literal for the '<em><b>Distance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POSITION__DISTANCE = eINSTANCE.getPosition_Distance();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.ConstantsAgregatorImpl <em>Constants Agregator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.ConstantsAgregatorImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getConstantsAgregator()
		 * @generated
		 */
		EClass CONSTANTS_AGREGATOR = eINSTANCE.getConstantsAgregator();

		/**
		 * The meta object literal for the '<em><b>Function</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANTS_AGREGATOR__FUNCTION = eINSTANCE.getConstantsAgregator_Function();

		/**
		 * The meta object literal for the '<em><b>Scale</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANTS_AGREGATOR__SCALE = eINSTANCE.getConstantsAgregator_Scale();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANTS_AGREGATOR__POSITION = eINSTANCE.getConstantsAgregator_Position();

		/**
		 * The meta object literal for the '<em><b>Chartvariable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANTS_AGREGATOR__CHARTVARIABLE = eINSTANCE.getConstantsAgregator_Chartvariable();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.ProjectImpl <em>Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.ProjectImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getProject()
		 * @generated
		 */
		EClass PROJECT = eINSTANCE.getProject();

		/**
		 * The meta object literal for the '<em><b>Dashboard</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROJECT__DASHBOARD = eINSTANCE.getProject_Dashboard();

		/**
		 * The meta object literal for the '<em><b>Constantsagregator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROJECT__CONSTANTSAGREGATOR = eINSTANCE.getProject_Constantsagregator();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT__ID = eINSTANCE.getProject_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT__NAME = eINSTANCE.getProject_Name();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.VariableStyleImpl <em>Variable Style</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.VariableStyleImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getVariableStyle()
		 * @generated
		 */
		EClass VARIABLE_STYLE = eINSTANCE.getVariableStyle();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_STYLE__COLOR = eINSTANCE.getVariableStyle_Color();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.BarImpl <em>Bar</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.BarImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getBar()
		 * @generated
		 */
		EClass BAR = eINSTANCE.getBar();

		/**
		 * The meta object literal for the '<em><b>Orientation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAR__ORIENTATION = eINSTANCE.getBar_Orientation();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.LineImpl <em>Line</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.LineImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getLine()
		 * @generated
		 */
		EClass LINE = eINSTANCE.getLine();

		/**
		 * The meta object literal for the '<em><b>Fill</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINE__FILL = eINSTANCE.getLine_Fill();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.PointImpl <em>Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.PointImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getPoint()
		 * @generated
		 */
		EClass POINT = eINSTANCE.getPoint();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.PageImpl <em>Page</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.PageImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getPage()
		 * @generated
		 */
		EClass PAGE = eINSTANCE.getPage();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAGE__NAME = eINSTANCE.getPage_Name();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAGE__CONTAINER = eINSTANCE.getPage_Container();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.ContentImpl <em>Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.ContentImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getContent()
		 * @generated
		 */
		EClass CONTENT = eINSTANCE.getContent();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTENT__ID = eINSTANCE.getContent_Id();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.AxisImpl <em>Axis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.AxisImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getAxis()
		 * @generated
		 */
		EClass AXIS = eINSTANCE.getAxis();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AXIS__MIN = eINSTANCE.getAxis_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AXIS__MAX = eINSTANCE.getAxis_Max();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AXIS__TITLE = eINSTANCE.getAxis_Title();

		/**
		 * The meta object literal for the '<em><b>Labels</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AXIS__LABELS = eINSTANCE.getAxis_Labels();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.xAxisImpl <em>xAxis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.xAxisImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getxAxis()
		 * @generated
		 */
		EClass XAXIS = eINSTANCE.getxAxis();

		/**
		 * The meta object literal for the '{@link DaShboardL.impl.yAxisImpl <em>yAxis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DaShboardL.impl.yAxisImpl
		 * @see DaShboardL.impl.DaShboardLPackageImpl#getyAxis()
		 * @generated
		 */
		EClass YAXIS = eINSTANCE.getyAxis();

	}

} //DaShboardLPackage
