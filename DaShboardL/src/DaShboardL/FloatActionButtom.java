/**
 */
package DaShboardL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Action Buttom</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.FloatActionButtom#getIcon <em>Icon</em>}</li>
 * </ul>
 *
 * @see DaShboardL.DaShboardLPackage#getFloatActionButtom()
 * @model
 * @generated
 */
public interface FloatActionButtom extends Style {
	/**
	 * Returns the value of the '<em><b>Icon</b></em>' attribute.
	 * The default value is <code>"default_fab"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icon</em>' attribute.
	 * @see #setIcon(String)
	 * @see DaShboardL.DaShboardLPackage#getFloatActionButtom_Icon()
	 * @model default="default_fab" required="true"
	 * @generated
	 */
	String getIcon();

	/**
	 * Sets the value of the '{@link DaShboardL.FloatActionButtom#getIcon <em>Icon</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon</em>' attribute.
	 * @see #getIcon()
	 * @generated
	 */
	void setIcon(String value);

} // FloatActionButtom
