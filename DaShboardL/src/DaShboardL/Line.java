/**
 */
package DaShboardL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Line</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.Line#isFill <em>Fill</em>}</li>
 * </ul>
 *
 * @see DaShboardL.DaShboardLPackage#getLine()
 * @model
 * @generated
 */
public interface Line extends VariableStyle {
	/**
	 * Returns the value of the '<em><b>Fill</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fill</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fill</em>' attribute.
	 * @see #setFill(boolean)
	 * @see DaShboardL.DaShboardLPackage#getLine_Fill()
	 * @model
	 * @generated
	 */
	boolean isFill();

	/**
	 * Sets the value of the '{@link DaShboardL.Line#isFill <em>Fill</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fill</em>' attribute.
	 * @see #isFill()
	 * @generated
	 */
	void setFill(boolean value);

} // Line
