/**
 */
package DaShboardL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.List#getOrientation <em>Orientation</em>}</li>
 *   <li>{@link DaShboardL.List#isAppear <em>Appear</em>}</li>
 * </ul>
 *
 * @see DaShboardL.DaShboardLPackage#getList()
 * @model
 * @generated
 */
public interface List extends Style {
	/**
	 * Returns the value of the '<em><b>Orientation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Orientation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Orientation</em>' attribute.
	 * @see #setOrientation(char)
	 * @see DaShboardL.DaShboardLPackage#getList_Orientation()
	 * @model required="true"
	 * @generated
	 */
	char getOrientation();

	/**
	 * Sets the value of the '{@link DaShboardL.List#getOrientation <em>Orientation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Orientation</em>' attribute.
	 * @see #getOrientation()
	 * @generated
	 */
	void setOrientation(char value);

	/**
	 * Returns the value of the '<em><b>Appear</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Appear</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Appear</em>' attribute.
	 * @see #setAppear(boolean)
	 * @see DaShboardL.DaShboardLPackage#getList_Appear()
	 * @model
	 * @generated
	 */
	boolean isAppear();

	/**
	 * Sets the value of the '{@link DaShboardL.List#isAppear <em>Appear</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Appear</em>' attribute.
	 * @see #isAppear()
	 * @generated
	 */
	void setAppear(boolean value);

} // List
