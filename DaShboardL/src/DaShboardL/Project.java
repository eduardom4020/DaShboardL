/**
 */
package DaShboardL;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.Project#getDashboard <em>Dashboard</em>}</li>
 *   <li>{@link DaShboardL.Project#getConstantsagregator <em>Constantsagregator</em>}</li>
 *   <li>{@link DaShboardL.Project#getId <em>Id</em>}</li>
 *   <li>{@link DaShboardL.Project#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see DaShboardL.DaShboardLPackage#getProject()
 * @model
 * @generated
 */
public interface Project extends EObject {
	/**
	 * Returns the value of the '<em><b>Dashboard</b></em>' containment reference list.
	 * The list contents are of type {@link DaShboardL.Dashboard}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dashboard</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dashboard</em>' containment reference list.
	 * @see DaShboardL.DaShboardLPackage#getProject_Dashboard()
	 * @model containment="true"
	 * @generated
	 */
	EList<Dashboard> getDashboard();

	/**
	 * Returns the value of the '<em><b>Constantsagregator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constantsagregator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constantsagregator</em>' containment reference.
	 * @see #setConstantsagregator(ConstantsAgregator)
	 * @see DaShboardL.DaShboardLPackage#getProject_Constantsagregator()
	 * @model containment="true"
	 * @generated
	 */
	ConstantsAgregator getConstantsagregator();

	/**
	 * Sets the value of the '{@link DaShboardL.Project#getConstantsagregator <em>Constantsagregator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constantsagregator</em>' containment reference.
	 * @see #getConstantsagregator()
	 * @generated
	 */
	void setConstantsagregator(ConstantsAgregator value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see DaShboardL.DaShboardLPackage#getProject_Id()
	 * @model
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link DaShboardL.Project#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see DaShboardL.DaShboardLPackage#getProject_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link DaShboardL.Project#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Project
