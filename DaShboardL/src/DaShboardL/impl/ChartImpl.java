/**
 */
package DaShboardL.impl;

import DaShboardL.Chart;
import DaShboardL.ChartVariable;
import DaShboardL.DaShboardLPackage;
import DaShboardL.Tooltip;

import DaShboardL.xAxis;
import DaShboardL.yAxis;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Chart</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.impl.ChartImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link DaShboardL.impl.ChartImpl#getSubtitle <em>Subtitle</em>}</li>
 *   <li>{@link DaShboardL.impl.ChartImpl#getTooltip <em>Tooltip</em>}</li>
 *   <li>{@link DaShboardL.impl.ChartImpl#getChart_get_variable <em>Chart get variable</em>}</li>
 *   <li>{@link DaShboardL.impl.ChartImpl#getXaxis <em>Xaxis</em>}</li>
 *   <li>{@link DaShboardL.impl.ChartImpl#getYaxis <em>Yaxis</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ChartImpl extends ContentImpl implements Chart {
	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSubtitle() <em>Subtitle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtitle()
	 * @generated
	 * @ordered
	 */
	protected static final String SUBTITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSubtitle() <em>Subtitle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtitle()
	 * @generated
	 * @ordered
	 */
	protected String subtitle = SUBTITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTooltip() <em>Tooltip</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTooltip()
	 * @generated
	 * @ordered
	 */
	protected EList<Tooltip> tooltip;

	/**
	 * The cached value of the '{@link #getChart_get_variable() <em>Chart get variable</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChart_get_variable()
	 * @generated
	 * @ordered
	 */
	protected EList<ChartVariable> chart_get_variable;

	/**
	 * The cached value of the '{@link #getXaxis() <em>Xaxis</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXaxis()
	 * @generated
	 * @ordered
	 */
	protected xAxis xaxis;

	/**
	 * The cached value of the '{@link #getYaxis() <em>Yaxis</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYaxis()
	 * @generated
	 * @ordered
	 */
	protected yAxis yaxis;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChartImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DaShboardLPackage.Literals.CHART;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSubtitle() {
		return subtitle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubtitle(String newSubtitle) {
		String oldSubtitle = subtitle;
		subtitle = newSubtitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART__SUBTITLE, oldSubtitle, subtitle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Tooltip> getTooltip() {
		if (tooltip == null) {
			tooltip = new EObjectContainmentEList<Tooltip>(Tooltip.class, this, DaShboardLPackage.CHART__TOOLTIP);
		}
		return tooltip;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ChartVariable> getChart_get_variable() {
		if (chart_get_variable == null) {
			chart_get_variable = new EObjectResolvingEList<ChartVariable>(ChartVariable.class, this, DaShboardLPackage.CHART__CHART_GET_VARIABLE);
		}
		return chart_get_variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public xAxis getXaxis() {
		return xaxis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetXaxis(xAxis newXaxis, NotificationChain msgs) {
		xAxis oldXaxis = xaxis;
		xaxis = newXaxis;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART__XAXIS, oldXaxis, newXaxis);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setXaxis(xAxis newXaxis) {
		if (newXaxis != xaxis) {
			NotificationChain msgs = null;
			if (xaxis != null)
				msgs = ((InternalEObject)xaxis).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DaShboardLPackage.CHART__XAXIS, null, msgs);
			if (newXaxis != null)
				msgs = ((InternalEObject)newXaxis).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DaShboardLPackage.CHART__XAXIS, null, msgs);
			msgs = basicSetXaxis(newXaxis, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART__XAXIS, newXaxis, newXaxis));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public yAxis getYaxis() {
		return yaxis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetYaxis(yAxis newYaxis, NotificationChain msgs) {
		yAxis oldYaxis = yaxis;
		yaxis = newYaxis;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART__YAXIS, oldYaxis, newYaxis);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setYaxis(yAxis newYaxis) {
		if (newYaxis != yaxis) {
			NotificationChain msgs = null;
			if (yaxis != null)
				msgs = ((InternalEObject)yaxis).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DaShboardLPackage.CHART__YAXIS, null, msgs);
			if (newYaxis != null)
				msgs = ((InternalEObject)newYaxis).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DaShboardLPackage.CHART__YAXIS, null, msgs);
			msgs = basicSetYaxis(newYaxis, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART__YAXIS, newYaxis, newYaxis));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DaShboardLPackage.CHART__TOOLTIP:
				return ((InternalEList<?>)getTooltip()).basicRemove(otherEnd, msgs);
			case DaShboardLPackage.CHART__XAXIS:
				return basicSetXaxis(null, msgs);
			case DaShboardLPackage.CHART__YAXIS:
				return basicSetYaxis(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DaShboardLPackage.CHART__TITLE:
				return getTitle();
			case DaShboardLPackage.CHART__SUBTITLE:
				return getSubtitle();
			case DaShboardLPackage.CHART__TOOLTIP:
				return getTooltip();
			case DaShboardLPackage.CHART__CHART_GET_VARIABLE:
				return getChart_get_variable();
			case DaShboardLPackage.CHART__XAXIS:
				return getXaxis();
			case DaShboardLPackage.CHART__YAXIS:
				return getYaxis();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DaShboardLPackage.CHART__TITLE:
				setTitle((String)newValue);
				return;
			case DaShboardLPackage.CHART__SUBTITLE:
				setSubtitle((String)newValue);
				return;
			case DaShboardLPackage.CHART__TOOLTIP:
				getTooltip().clear();
				getTooltip().addAll((Collection<? extends Tooltip>)newValue);
				return;
			case DaShboardLPackage.CHART__CHART_GET_VARIABLE:
				getChart_get_variable().clear();
				getChart_get_variable().addAll((Collection<? extends ChartVariable>)newValue);
				return;
			case DaShboardLPackage.CHART__XAXIS:
				setXaxis((xAxis)newValue);
				return;
			case DaShboardLPackage.CHART__YAXIS:
				setYaxis((yAxis)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DaShboardLPackage.CHART__TITLE:
				setTitle(TITLE_EDEFAULT);
				return;
			case DaShboardLPackage.CHART__SUBTITLE:
				setSubtitle(SUBTITLE_EDEFAULT);
				return;
			case DaShboardLPackage.CHART__TOOLTIP:
				getTooltip().clear();
				return;
			case DaShboardLPackage.CHART__CHART_GET_VARIABLE:
				getChart_get_variable().clear();
				return;
			case DaShboardLPackage.CHART__XAXIS:
				setXaxis((xAxis)null);
				return;
			case DaShboardLPackage.CHART__YAXIS:
				setYaxis((yAxis)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DaShboardLPackage.CHART__TITLE:
				return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
			case DaShboardLPackage.CHART__SUBTITLE:
				return SUBTITLE_EDEFAULT == null ? subtitle != null : !SUBTITLE_EDEFAULT.equals(subtitle);
			case DaShboardLPackage.CHART__TOOLTIP:
				return tooltip != null && !tooltip.isEmpty();
			case DaShboardLPackage.CHART__CHART_GET_VARIABLE:
				return chart_get_variable != null && !chart_get_variable.isEmpty();
			case DaShboardLPackage.CHART__XAXIS:
				return xaxis != null;
			case DaShboardLPackage.CHART__YAXIS:
				return yaxis != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (title: ");
		result.append(title);
		result.append(", subtitle: ");
		result.append(subtitle);
		result.append(')');
		return result.toString();
	}

} //ChartImpl
