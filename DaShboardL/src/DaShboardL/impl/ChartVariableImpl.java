/**
 */
package DaShboardL.impl;

import DaShboardL.ChartVariable;
import DaShboardL.DaShboardLPackage;
import DaShboardL.VariableStyle;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Chart Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.impl.ChartVariableImpl#getId <em>Id</em>}</li>
 *   <li>{@link DaShboardL.impl.ChartVariableImpl#getName <em>Name</em>}</li>
 *   <li>{@link DaShboardL.impl.ChartVariableImpl#getMeasure <em>Measure</em>}</li>
 *   <li>{@link DaShboardL.impl.ChartVariableImpl#getVariablestyle <em>Variablestyle</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ChartVariableImpl extends MinimalEObjectImpl.Container implements ChartVariable {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMeasure() <em>Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMeasure() <em>Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeasure()
	 * @generated
	 * @ordered
	 */
	protected String measure = MEASURE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getVariablestyle() <em>Variablestyle</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariablestyle()
	 * @generated
	 * @ordered
	 */
	protected VariableStyle variablestyle;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChartVariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DaShboardLPackage.Literals.CHART_VARIABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART_VARIABLE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART_VARIABLE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMeasure() {
		return measure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeasure(String newMeasure) {
		String oldMeasure = measure;
		measure = newMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART_VARIABLE__MEASURE, oldMeasure, measure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableStyle getVariablestyle() {
		return variablestyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariablestyle(VariableStyle newVariablestyle, NotificationChain msgs) {
		VariableStyle oldVariablestyle = variablestyle;
		variablestyle = newVariablestyle;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART_VARIABLE__VARIABLESTYLE, oldVariablestyle, newVariablestyle);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariablestyle(VariableStyle newVariablestyle) {
		if (newVariablestyle != variablestyle) {
			NotificationChain msgs = null;
			if (variablestyle != null)
				msgs = ((InternalEObject)variablestyle).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DaShboardLPackage.CHART_VARIABLE__VARIABLESTYLE, null, msgs);
			if (newVariablestyle != null)
				msgs = ((InternalEObject)newVariablestyle).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DaShboardLPackage.CHART_VARIABLE__VARIABLESTYLE, null, msgs);
			msgs = basicSetVariablestyle(newVariablestyle, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CHART_VARIABLE__VARIABLESTYLE, newVariablestyle, newVariablestyle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DaShboardLPackage.CHART_VARIABLE__VARIABLESTYLE:
				return basicSetVariablestyle(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DaShboardLPackage.CHART_VARIABLE__ID:
				return getId();
			case DaShboardLPackage.CHART_VARIABLE__NAME:
				return getName();
			case DaShboardLPackage.CHART_VARIABLE__MEASURE:
				return getMeasure();
			case DaShboardLPackage.CHART_VARIABLE__VARIABLESTYLE:
				return getVariablestyle();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DaShboardLPackage.CHART_VARIABLE__ID:
				setId((Integer)newValue);
				return;
			case DaShboardLPackage.CHART_VARIABLE__NAME:
				setName((String)newValue);
				return;
			case DaShboardLPackage.CHART_VARIABLE__MEASURE:
				setMeasure((String)newValue);
				return;
			case DaShboardLPackage.CHART_VARIABLE__VARIABLESTYLE:
				setVariablestyle((VariableStyle)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DaShboardLPackage.CHART_VARIABLE__ID:
				setId(ID_EDEFAULT);
				return;
			case DaShboardLPackage.CHART_VARIABLE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DaShboardLPackage.CHART_VARIABLE__MEASURE:
				setMeasure(MEASURE_EDEFAULT);
				return;
			case DaShboardLPackage.CHART_VARIABLE__VARIABLESTYLE:
				setVariablestyle((VariableStyle)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DaShboardLPackage.CHART_VARIABLE__ID:
				return id != ID_EDEFAULT;
			case DaShboardLPackage.CHART_VARIABLE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DaShboardLPackage.CHART_VARIABLE__MEASURE:
				return MEASURE_EDEFAULT == null ? measure != null : !MEASURE_EDEFAULT.equals(measure);
			case DaShboardLPackage.CHART_VARIABLE__VARIABLESTYLE:
				return variablestyle != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(", measure: ");
		result.append(measure);
		result.append(')');
		return result.toString();
	}

} //ChartVariableImpl
