/**
 */
package DaShboardL.impl;

import DaShboardL.ChartVariable;
import DaShboardL.ConstantsAgregator;
import DaShboardL.DaShboardLPackage;
import DaShboardL.Function;
import DaShboardL.Position;
import DaShboardL.Scale;

import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constants Agregator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.impl.ConstantsAgregatorImpl#getFunction <em>Function</em>}</li>
 *   <li>{@link DaShboardL.impl.ConstantsAgregatorImpl#getScale <em>Scale</em>}</li>
 *   <li>{@link DaShboardL.impl.ConstantsAgregatorImpl#getPosition <em>Position</em>}</li>
 *   <li>{@link DaShboardL.impl.ConstantsAgregatorImpl#getChartvariable <em>Chartvariable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConstantsAgregatorImpl extends MinimalEObjectImpl.Container implements ConstantsAgregator {
	/**
	 * The cached value of the '{@link #getFunction() <em>Function</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunction()
	 * @generated
	 * @ordered
	 */
	protected EList<Function> function;

	/**
	 * The cached value of the '{@link #getScale() <em>Scale</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScale()
	 * @generated
	 * @ordered
	 */
	protected EList<Scale> scale;

	/**
	 * The cached value of the '{@link #getPosition() <em>Position</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected EList<Position> position;

	/**
	 * The cached value of the '{@link #getChartvariable() <em>Chartvariable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChartvariable()
	 * @generated
	 * @ordered
	 */
	protected EList<ChartVariable> chartvariable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstantsAgregatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DaShboardLPackage.Literals.CONSTANTS_AGREGATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Function> getFunction() {
		if (function == null) {
			function = new EObjectContainmentEList<Function>(Function.class, this, DaShboardLPackage.CONSTANTS_AGREGATOR__FUNCTION);
		}
		return function;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Scale> getScale() {
		if (scale == null) {
			scale = new EObjectContainmentEList<Scale>(Scale.class, this, DaShboardLPackage.CONSTANTS_AGREGATOR__SCALE);
		}
		return scale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Position> getPosition() {
		if (position == null) {
			position = new EObjectContainmentEList<Position>(Position.class, this, DaShboardLPackage.CONSTANTS_AGREGATOR__POSITION);
		}
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ChartVariable> getChartvariable() {
		if (chartvariable == null) {
			chartvariable = new EObjectContainmentEList<ChartVariable>(ChartVariable.class, this, DaShboardLPackage.CONSTANTS_AGREGATOR__CHARTVARIABLE);
		}
		return chartvariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DaShboardLPackage.CONSTANTS_AGREGATOR__FUNCTION:
				return ((InternalEList<?>)getFunction()).basicRemove(otherEnd, msgs);
			case DaShboardLPackage.CONSTANTS_AGREGATOR__SCALE:
				return ((InternalEList<?>)getScale()).basicRemove(otherEnd, msgs);
			case DaShboardLPackage.CONSTANTS_AGREGATOR__POSITION:
				return ((InternalEList<?>)getPosition()).basicRemove(otherEnd, msgs);
			case DaShboardLPackage.CONSTANTS_AGREGATOR__CHARTVARIABLE:
				return ((InternalEList<?>)getChartvariable()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DaShboardLPackage.CONSTANTS_AGREGATOR__FUNCTION:
				return getFunction();
			case DaShboardLPackage.CONSTANTS_AGREGATOR__SCALE:
				return getScale();
			case DaShboardLPackage.CONSTANTS_AGREGATOR__POSITION:
				return getPosition();
			case DaShboardLPackage.CONSTANTS_AGREGATOR__CHARTVARIABLE:
				return getChartvariable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DaShboardLPackage.CONSTANTS_AGREGATOR__FUNCTION:
				getFunction().clear();
				getFunction().addAll((Collection<? extends Function>)newValue);
				return;
			case DaShboardLPackage.CONSTANTS_AGREGATOR__SCALE:
				getScale().clear();
				getScale().addAll((Collection<? extends Scale>)newValue);
				return;
			case DaShboardLPackage.CONSTANTS_AGREGATOR__POSITION:
				getPosition().clear();
				getPosition().addAll((Collection<? extends Position>)newValue);
				return;
			case DaShboardLPackage.CONSTANTS_AGREGATOR__CHARTVARIABLE:
				getChartvariable().clear();
				getChartvariable().addAll((Collection<? extends ChartVariable>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DaShboardLPackage.CONSTANTS_AGREGATOR__FUNCTION:
				getFunction().clear();
				return;
			case DaShboardLPackage.CONSTANTS_AGREGATOR__SCALE:
				getScale().clear();
				return;
			case DaShboardLPackage.CONSTANTS_AGREGATOR__POSITION:
				getPosition().clear();
				return;
			case DaShboardLPackage.CONSTANTS_AGREGATOR__CHARTVARIABLE:
				getChartvariable().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DaShboardLPackage.CONSTANTS_AGREGATOR__FUNCTION:
				return function != null && !function.isEmpty();
			case DaShboardLPackage.CONSTANTS_AGREGATOR__SCALE:
				return scale != null && !scale.isEmpty();
			case DaShboardLPackage.CONSTANTS_AGREGATOR__POSITION:
				return position != null && !position.isEmpty();
			case DaShboardLPackage.CONSTANTS_AGREGATOR__CHARTVARIABLE:
				return chartvariable != null && !chartvariable.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConstantsAgregatorImpl
