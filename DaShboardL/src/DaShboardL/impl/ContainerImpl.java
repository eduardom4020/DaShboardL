/**
 */
package DaShboardL.impl;

import DaShboardL.Content;
import DaShboardL.DaShboardLPackage;
import DaShboardL.Position;
import DaShboardL.Scale;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.impl.ContainerImpl#getId <em>Id</em>}</li>
 *   <li>{@link DaShboardL.impl.ContainerImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link DaShboardL.impl.ContainerImpl#isLabel_hidden <em>Label hidden</em>}</li>
 *   <li>{@link DaShboardL.impl.ContainerImpl#getPosition <em>Position</em>}</li>
 *   <li>{@link DaShboardL.impl.ContainerImpl#getScale <em>Scale</em>}</li>
 *   <li>{@link DaShboardL.impl.ContainerImpl#getContent <em>Content</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ContainerImpl extends MinimalEObjectImpl.Container implements DaShboardL.Container {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #isLabel_hidden() <em>Label hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLabel_hidden()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LABEL_HIDDEN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isLabel_hidden() <em>Label hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLabel_hidden()
	 * @generated
	 * @ordered
	 */
	protected boolean label_hidden = LABEL_HIDDEN_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPosition() <em>Position</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected EList<Position> position;

	/**
	 * The cached value of the '{@link #getScale() <em>Scale</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScale()
	 * @generated
	 * @ordered
	 */
	protected EList<Scale> scale;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected Content content;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DaShboardLPackage.Literals.CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CONTAINER__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CONTAINER__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLabel_hidden() {
		return label_hidden;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel_hidden(boolean newLabel_hidden) {
		boolean oldLabel_hidden = label_hidden;
		label_hidden = newLabel_hidden;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CONTAINER__LABEL_HIDDEN, oldLabel_hidden, label_hidden));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Position> getPosition() {
		if (position == null) {
			position = new EObjectResolvingEList<Position>(Position.class, this, DaShboardLPackage.CONTAINER__POSITION);
		}
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Scale> getScale() {
		if (scale == null) {
			scale = new EObjectResolvingEList<Scale>(Scale.class, this, DaShboardLPackage.CONTAINER__SCALE);
		}
		return scale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Content getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContent(Content newContent, NotificationChain msgs) {
		Content oldContent = content;
		content = newContent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CONTAINER__CONTENT, oldContent, newContent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(Content newContent) {
		if (newContent != content) {
			NotificationChain msgs = null;
			if (content != null)
				msgs = ((InternalEObject)content).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DaShboardLPackage.CONTAINER__CONTENT, null, msgs);
			if (newContent != null)
				msgs = ((InternalEObject)newContent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DaShboardLPackage.CONTAINER__CONTENT, null, msgs);
			msgs = basicSetContent(newContent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.CONTAINER__CONTENT, newContent, newContent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DaShboardLPackage.CONTAINER__CONTENT:
				return basicSetContent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DaShboardLPackage.CONTAINER__ID:
				return getId();
			case DaShboardLPackage.CONTAINER__LABEL:
				return getLabel();
			case DaShboardLPackage.CONTAINER__LABEL_HIDDEN:
				return isLabel_hidden();
			case DaShboardLPackage.CONTAINER__POSITION:
				return getPosition();
			case DaShboardLPackage.CONTAINER__SCALE:
				return getScale();
			case DaShboardLPackage.CONTAINER__CONTENT:
				return getContent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DaShboardLPackage.CONTAINER__ID:
				setId((Integer)newValue);
				return;
			case DaShboardLPackage.CONTAINER__LABEL:
				setLabel((String)newValue);
				return;
			case DaShboardLPackage.CONTAINER__LABEL_HIDDEN:
				setLabel_hidden((Boolean)newValue);
				return;
			case DaShboardLPackage.CONTAINER__POSITION:
				getPosition().clear();
				getPosition().addAll((Collection<? extends Position>)newValue);
				return;
			case DaShboardLPackage.CONTAINER__SCALE:
				getScale().clear();
				getScale().addAll((Collection<? extends Scale>)newValue);
				return;
			case DaShboardLPackage.CONTAINER__CONTENT:
				setContent((Content)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DaShboardLPackage.CONTAINER__ID:
				setId(ID_EDEFAULT);
				return;
			case DaShboardLPackage.CONTAINER__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case DaShboardLPackage.CONTAINER__LABEL_HIDDEN:
				setLabel_hidden(LABEL_HIDDEN_EDEFAULT);
				return;
			case DaShboardLPackage.CONTAINER__POSITION:
				getPosition().clear();
				return;
			case DaShboardLPackage.CONTAINER__SCALE:
				getScale().clear();
				return;
			case DaShboardLPackage.CONTAINER__CONTENT:
				setContent((Content)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DaShboardLPackage.CONTAINER__ID:
				return id != ID_EDEFAULT;
			case DaShboardLPackage.CONTAINER__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case DaShboardLPackage.CONTAINER__LABEL_HIDDEN:
				return label_hidden != LABEL_HIDDEN_EDEFAULT;
			case DaShboardLPackage.CONTAINER__POSITION:
				return position != null && !position.isEmpty();
			case DaShboardLPackage.CONTAINER__SCALE:
				return scale != null && !scale.isEmpty();
			case DaShboardLPackage.CONTAINER__CONTENT:
				return content != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", label: ");
		result.append(label);
		result.append(", label_hidden: ");
		result.append(label_hidden);
		result.append(')');
		return result.toString();
	}

} //ContainerImpl
