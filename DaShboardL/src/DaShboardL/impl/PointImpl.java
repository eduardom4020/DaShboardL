/**
 */
package DaShboardL.impl;

import DaShboardL.DaShboardLPackage;
import DaShboardL.Point;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PointImpl extends VariableStyleImpl implements Point {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PointImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DaShboardLPackage.Literals.POINT;
	}

} //PointImpl
