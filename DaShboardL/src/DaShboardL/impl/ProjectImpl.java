/**
 */
package DaShboardL.impl;

import DaShboardL.ConstantsAgregator;
import DaShboardL.DaShboardLPackage;
import DaShboardL.Dashboard;
import DaShboardL.Project;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DaShboardL.impl.ProjectImpl#getDashboard <em>Dashboard</em>}</li>
 *   <li>{@link DaShboardL.impl.ProjectImpl#getConstantsagregator <em>Constantsagregator</em>}</li>
 *   <li>{@link DaShboardL.impl.ProjectImpl#getId <em>Id</em>}</li>
 *   <li>{@link DaShboardL.impl.ProjectImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProjectImpl extends MinimalEObjectImpl.Container implements Project {
	/**
	 * The cached value of the '{@link #getDashboard() <em>Dashboard</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDashboard()
	 * @generated
	 * @ordered
	 */
	protected EList<Dashboard> dashboard;

	/**
	 * The cached value of the '{@link #getConstantsagregator() <em>Constantsagregator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantsagregator()
	 * @generated
	 * @ordered
	 */
	protected ConstantsAgregator constantsagregator;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DaShboardLPackage.Literals.PROJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dashboard> getDashboard() {
		if (dashboard == null) {
			dashboard = new EObjectContainmentEList<Dashboard>(Dashboard.class, this, DaShboardLPackage.PROJECT__DASHBOARD);
		}
		return dashboard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantsAgregator getConstantsagregator() {
		return constantsagregator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstantsagregator(ConstantsAgregator newConstantsagregator, NotificationChain msgs) {
		ConstantsAgregator oldConstantsagregator = constantsagregator;
		constantsagregator = newConstantsagregator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DaShboardLPackage.PROJECT__CONSTANTSAGREGATOR, oldConstantsagregator, newConstantsagregator);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstantsagregator(ConstantsAgregator newConstantsagregator) {
		if (newConstantsagregator != constantsagregator) {
			NotificationChain msgs = null;
			if (constantsagregator != null)
				msgs = ((InternalEObject)constantsagregator).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DaShboardLPackage.PROJECT__CONSTANTSAGREGATOR, null, msgs);
			if (newConstantsagregator != null)
				msgs = ((InternalEObject)newConstantsagregator).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DaShboardLPackage.PROJECT__CONSTANTSAGREGATOR, null, msgs);
			msgs = basicSetConstantsagregator(newConstantsagregator, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.PROJECT__CONSTANTSAGREGATOR, newConstantsagregator, newConstantsagregator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.PROJECT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DaShboardLPackage.PROJECT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DaShboardLPackage.PROJECT__DASHBOARD:
				return ((InternalEList<?>)getDashboard()).basicRemove(otherEnd, msgs);
			case DaShboardLPackage.PROJECT__CONSTANTSAGREGATOR:
				return basicSetConstantsagregator(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DaShboardLPackage.PROJECT__DASHBOARD:
				return getDashboard();
			case DaShboardLPackage.PROJECT__CONSTANTSAGREGATOR:
				return getConstantsagregator();
			case DaShboardLPackage.PROJECT__ID:
				return getId();
			case DaShboardLPackage.PROJECT__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DaShboardLPackage.PROJECT__DASHBOARD:
				getDashboard().clear();
				getDashboard().addAll((Collection<? extends Dashboard>)newValue);
				return;
			case DaShboardLPackage.PROJECT__CONSTANTSAGREGATOR:
				setConstantsagregator((ConstantsAgregator)newValue);
				return;
			case DaShboardLPackage.PROJECT__ID:
				setId((Integer)newValue);
				return;
			case DaShboardLPackage.PROJECT__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DaShboardLPackage.PROJECT__DASHBOARD:
				getDashboard().clear();
				return;
			case DaShboardLPackage.PROJECT__CONSTANTSAGREGATOR:
				setConstantsagregator((ConstantsAgregator)null);
				return;
			case DaShboardLPackage.PROJECT__ID:
				setId(ID_EDEFAULT);
				return;
			case DaShboardLPackage.PROJECT__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DaShboardLPackage.PROJECT__DASHBOARD:
				return dashboard != null && !dashboard.isEmpty();
			case DaShboardLPackage.PROJECT__CONSTANTSAGREGATOR:
				return constantsagregator != null;
			case DaShboardLPackage.PROJECT__ID:
				return id != ID_EDEFAULT;
			case DaShboardLPackage.PROJECT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ProjectImpl
