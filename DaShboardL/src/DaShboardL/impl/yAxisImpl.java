/**
 */
package DaShboardL.impl;

import DaShboardL.DaShboardLPackage;
import DaShboardL.yAxis;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>yAxis</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class yAxisImpl extends AxisImpl implements yAxis {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected yAxisImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DaShboardLPackage.Literals.YAXIS;
	}

} //yAxisImpl
