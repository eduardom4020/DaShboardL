# DaShboardL

DaShboardL, ou DaShL é uma DSL criada com finalidade de fornecer um mecanismo prático e rápido de gerar Dashboards. Visto que o objetivo principal destas aplicações é expor dados aos clientes de forma mais clara, através de visualizações, em geral perde-se muito tempo com o desenvolvimento do front end, quando na verdade este nem é o foco principal da aplicação. Além disso, mesmo tendo padrões bem definidos, muitas vezes é necessário muito retrabalho para a criação de novos dashboards.
Sendo assim, a linguagem fornece uma maneira facilitada de gerar toda a extrutura do dashboard, com frontend implementado em react, um simples porém eficaz backend em flask, com rotas que podem ser endpoints para obtenção de dados a partir de uma API.

## Configurando o Projeto

Acesse seu Obeo Designer e selecione a opção de abrir projeto. Para esta primeira instância selecione as pastas DaShboardL e org.eclipse.acceleo.dashboardl. Na barra à esqueda, acesse DaShboardL/src/DaShboardL.genmodel. Clique com o botão direito e selecione a opção GenerateAll. Após o processo, aparecerão os arquivos .edit, .editor e .tests.
Agora mude a perspectiva do Obeo para Acceleo, clique direito em org.eclipse.acceleo.dashboardl e selecione a opção generate acceleo ui.
De volta ao projeto DaShboardL, clique com o botão direito e selecione run eclipse application. Nesta nova instância abra os projetos que estão na pasta runtim-Runtime.
Com isso, a configuração inicial do ambiente de desenvolvimento do projeto estará feita.

### Pré-Requisitos

Para o passo anterior e para os posteriores, vamos precisar instalar:	
	* Obeo Designer Comunity
	* Python3 e Pip3
	* Nodejs e npm através da NVM
	* virtualenv

## Os 3 Principais Componentes do Projeto

Segue uma breve explicação sobre as implementações dos três pilares de uma DSL:

### Metamodelo

Materializado através do arquivo ecore, este é uma sintaxe abstrata do tipo de aplicação que desejamos construir, no caso dashboards.
A organização do metamodelo deste trabalho consiste em um Project, que é um objeto que contém todos os outros objetos. Podemos ter vários dashboards e apenas um agregador de constantes. Com isso podemos ter tanto mais de um dashboard, quanto maior garantia que todos terão acesso aos mesmos dados.

### Acceleo

Compila o Metamodelo para algum resultado final. 

### Sirius

Gera a sintaxe concreta desta linguagem; Tudo aquilo que for voltado para ouso do cliente;

## Utilizando a Sintaxe Concreta

Acesse a instância utilizando o comando 

```
Run > Eclipse Application
```

Crie ou acesse um porojeto de modelagem. Em caso de criação, após criar o projeto crie um arquivo do tipo .dashboardl, clicando com o botão direito no projeto:

```
New > Other > Eclipse EMF > dashboardl
```

Selecione Project para ser o elemento raíz do arquivo. Após a criação, na janela à esquerda, acesse as setas deste arquivo .dashboardl até encontrar o arquivo Project. Clique com o botão direito e selecione a opção de utilizar ferramenta de modelagem.
Uma interface drag and drop será exibida, e você pode modelar seusdashboards livremente nela. 

## Obtendo Resultados

Após modelar seus dashboards, vá novamente em Project, do lado esquerdo, clique com o botão direito e acesse a opção 

```
Acceleo > Acceleo Model to Text
```

Uma mensagem será exibida e os arquivos gerados estarão em uma pasta com o nome do projeto, dentro de DaShboardL_Generated.

## Testando Seu Dashboard

*Para este passo assumimos que tdos os pré-requisitos foram instalados com sucesso.*

Cada dashboard gerado em um projeto é um diretório contendo frontend em react e backend em flask. Então para cada um teremos que levantar seu servidor. Para isso, em um terminal do linux acesse: 

```
Path para o projeto de modelagem > DaShboardL_Generated > Algum Dashboard
```

E execute o comando:

```
sh ./run.sh
```

Após o carregamento, acesse o dashboard no endereço indicado no terminal.

## Versões

	* v0.8 - Projeto migrado de Gogs para Gitlab
	* v0.9 - Versão inicial, porém sem testes (Atual)

## Requisitos Para a Próxima Versão
	* Adicionar mais uma opção de menu
	* Corrigir bugs nas funções do menu
	* Corrigir bugs na exibição de múltiplos gráficos
	* Realizar testes unitários
	* Realizar testes controlados de usabilidade

## Autor

**Eduardo Melo de Carvalho Braga**

## Licença

*Procurando uma boa...*
