from flask import Flask, render_template, make_response, request, jsonify, send_file, json
import random
import os

app = Flask(__name__, static_folder="../static/dist", template_folder="../static")
root = os.path.realpath(os.path.dirname(__file__))

@app.route('/Page_0', methods=['GET'])
@app.route('/Page_0', methods=['GET'])
def index():
	return render_template('index.html')

@app.route('/action_Function_1', methods=['POST'])
def action_Function_1():
	return make_response(jsonify({'code': 'redirect', 'route': 'Page_0'}, 200))
@app.route('/action_Function_2', methods=['POST'])
def action_Function_2():
	return make_response(jsonify({'code': 'redirect', 'route': 'Page_0'}, 200))

@app.route('/data_Variable_1', methods=['POST'])
def data_Variable_1():
	json_url = os.path.join(root, 'data', 'Variable_1.json')
	data = json.load(open(json_url))
	return make_response(jsonify(data), 200)
@app.route('/data_Variable_2', methods=['POST'])
def data_Variable_2():
	json_url = os.path.join(root, 'data', 'Variable_2.json')
	data = json.load(open(json_url))
	return make_response(jsonify(data), 200)

if __name__ == "__main__":
	app.run(debug=True, port=5000)
