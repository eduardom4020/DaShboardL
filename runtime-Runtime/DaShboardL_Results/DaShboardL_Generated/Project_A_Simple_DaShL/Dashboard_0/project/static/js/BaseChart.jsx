import React from 'react';
var $ = require('jquery');

import { 
	Row, Col,

} from 'reactstrap';

import Highcharts from 'highcharts/highstock';


export default class BaseChart extends React.Component {
	constructor(props) {
		super(props);

		console.log(this.props.series);

		this.state = {
			series: this.props.series,
			xAxis: this.props.xAxis,
			yAxis: this.props.yAxis,
		};

		this.buildChart = this.buildChart.bind(this);
		this.runSeries = this.runSeries.bind(this);
	}

	render() {
		return(
			<Row>
				<Col>
					<div id={this.props.id}></div>
				</Col>
			</Row>
		);
	}

	buildChart(highchart) {
		this.runSeries(series => 
			highchart = Highcharts.chart(this.props.id, {
			    title: {
			        text: this.props.title
			    },
			    subtitle: {
			        text: this.props.subtitle
			    },
			    xAxis: {
			    	min: this.state.xAxis.min,
			        max: this.state.xAxis.max,
			        categories: this.state.xAxis.labels,
			        title: {
			            text: this.state.xAxis.title
			        },
			        crosshair: true
			    },
			    yAxis: {
			        min: this.state.yAxis.min,
			        max: this.state.yAxis.max,
			        categories: this.state.yAxis.labels,
			        title: {
			            text: this.state.yAxis.title
			        }
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
			        footerFormat: '</table>',
			        shared: true,
			        useHTML: true
			    },
			    plotOptions: {
			        column: {
			            pointPadding: 0.2,
			            borderWidth: 0
			        }
			    },
			    series: series
			})
		);
	}

	runSeries(callback) {
		let output = [];
		let count = 0;

		for (const [key, value] of Object.entries(this.state.series)) {
			value.get_data()
					.done(res => {
							count += 1;
							output.push({
								type: value.type,
								name: key,
								data: res.value
							});
							if(count == Object.keys(this.state.series).length) {
								callback(output);
							}
						}
					)
		}
	}

	componentDidMount() {
		this.buildChart(window['highchart' + this.props.id]);
	}
}
