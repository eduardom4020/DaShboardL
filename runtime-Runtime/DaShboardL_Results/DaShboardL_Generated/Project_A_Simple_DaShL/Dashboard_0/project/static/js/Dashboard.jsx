import React from 'react';
var $ = require('jquery');

import Menu from './Menu';
import BaseChart from './BaseChart';

import { 
	Container

} from 'reactstrap';

const function_result = res => {
	if(res[1] == 200) {
		switch(res[0].code) {
			case 'redirect':
				window.location = [0].route;
			break;
		}
	}
};

const Page_0_page = <Container>
	<Container>
		<Menu
			style='dropdown'
		
			name='Menu'

			functions = {
				{
					'Function_2': {
						'action': () => $.post(location.origin + '/action_Function_2').done(function_result)
					},
				}
			}
		/>

	</Container>
	<Container>

		<BaseChart
			title='Chart'
			subtitle=''
			id='0'

			series = {
				{
					'Variable_2': {
						'type': 'column',
						'get_data': () => $.post(location.origin + '/data_Variable_2')
					}
				}
			}
			xAxis = {
				{
					'min': 0.0,
					'max': 100.0,
				}
			}
			yAxis = {
				{
					'min': 0.0,
					'max': 100.0,
				}
			}
		/>
	</Container>
</Container>;

export default class Dashboard extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			Page_0: Page_0_page,
		};
	}

	render() {
		return(this.state[this.props.page]);
	}
}
