var $ = require('jquery');

import React from 'react';
import { Router, Route, hashHistory, Switch } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import Dashboard from './Dashboard';

const newHistory = createBrowserHistory();

const Page_0_page = () => <Dashboard page="Page_0"/>;

export default class Project extends React.Component {
	constructor(props) {
		super(props);
	}

	render () {
		return (
			<Router history={newHistory}>
				<Switch>
					<Route path='/Page_0' component={Page_0_page} />
					
				</Switch>
			</Router>
		);
	}
}
