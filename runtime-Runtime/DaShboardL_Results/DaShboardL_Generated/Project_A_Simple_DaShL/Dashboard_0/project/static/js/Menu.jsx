import React from 'react';
var $ = require('jquery');

import { 
	Row, Col, 
	ButtonDropdown, DropdownToggle,
	DropdownMenu, DropdownItem

} from 'reactstrap';

export default class Menu extends React.Component {
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);

		console.log(this.props.functions);

		let dropdown = () => 
			<ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
			  <DropdownToggle caret size="lg">
			    {this.props.name}
			  </DropdownToggle>
			  <DropdownMenu>
			  		{Object.entries(this.props.functions).map( func => 
			  			<DropdownItem key={func[0]} onClick={func[1].action} >{func[0]}</DropdownItem>	
		  			)}
			  </DropdownMenu>
			</ButtonDropdown>
		;

	    this.state = {
	    	dropdown: dropdown,
			dropdownOpen: false
	    };
	}

	toggle() {
		this.setState({
			dropdownOpen: !this.state.dropdownOpen
		});
	}

	render() {
		return(
			<Row>
				<Col>
					{this.state[this.props.style]()}
				</Col>
			</Row>
		);
	}
}
