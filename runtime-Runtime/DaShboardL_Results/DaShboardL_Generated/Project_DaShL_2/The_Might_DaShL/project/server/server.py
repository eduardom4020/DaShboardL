from flask import Flask, render_template, make_response, request, jsonify, send_file, json
import random
import os

app = Flask(__name__, static_folder="../static/dist", template_folder="../static")
root = os.path.realpath(os.path.dirname(__file__))

@app.route('/', methods=['GET'])
@app.route('/scatter', methods=['GET'])
@app.route('/line', methods=['GET'])
def index():
	return render_template('index.html')

@app.route('/action_Line', methods=['POST'])
def action_Line():
	return make_response(jsonify({'code': 'redirect', 'route': 'line'}, 200))
@app.route('/action_Scatter', methods=['POST'])
def action_Scatter():
	return make_response(jsonify({'code': 'redirect', 'route': 'scatter'}, 200))

@app.route('/data_Some_Value', methods=['POST'])
def data_Some_Value():
	json_url = os.path.join(root, 'data', 'Some_Value.json')
	data = json.load(open(json_url))
	return make_response(jsonify(data), 200)
@app.route('/data_Another_Value', methods=['POST'])
def data_Another_Value():
	json_url = os.path.join(root, 'data', 'Another_Value.json')
	data = json.load(open(json_url))
	return make_response(jsonify(data), 200)
@app.route('/data_Speed', methods=['POST'])
def data_Speed():
	json_url = os.path.join(root, 'data', 'Speed.json')
	data = json.load(open(json_url))
	return make_response(jsonify(data), 200)
@app.route('/data_Bitcoin_Price', methods=['POST'])
def data_Bitcoin_Price():
	json_url = os.path.join(root, 'data', 'Bitcoin_Price.json')
	data = json.load(open(json_url))
	return make_response(jsonify(data), 200)
@app.route('/data_Money', methods=['POST'])
def data_Money():
	json_url = os.path.join(root, 'data', 'Money.json')
	data = json.load(open(json_url))
	return make_response(jsonify(data), 200)
@app.route('/data_Not_Slept_Hours', methods=['POST'])
def data_Not_Slept_Hours():
	json_url = os.path.join(root, 'data', 'Not_Slept_Hours.json')
	data = json.load(open(json_url))
	return make_response(jsonify(data), 200)

if __name__ == "__main__":
	app.run(debug=True, port=5000)
