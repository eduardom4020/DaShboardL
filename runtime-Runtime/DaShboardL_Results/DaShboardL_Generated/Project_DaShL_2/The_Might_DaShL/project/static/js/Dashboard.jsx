import React from 'react';
var $ = require('jquery');

import Menu from './Menu';
import BaseChart from './BaseChart';

import { 
	Container

} from 'reactstrap';

const function_result = res => {
	if(res[1] == 200) {
		switch(res[0].code) {
			case 'redirect':
				window.location = [0].route;
			break;
		}
	}
};

const main_page = <Container>
	<Container>

		<BaseChart
			title='Some Mixed Things'
			subtitle='This Chart Shows Mixed Plot Styles'
			id='0'

			series = {
				{
					'Speed': {
						'type': 'line',
						'get_data': () => $.post(location.origin + '/data_Speed')
					},
					'Some_Value': {
						'type': 'scatter',
						'get_data': () => $.post(location.origin + '/data_Some_Value')
					},
					'Another_Value': {
						'type': 'column',
						'get_data': () => $.post(location.origin + '/data_Another_Value')
					}
				}
			}
			xAxis = {
				{
					'min': 0.0,
					'max': 12.0,
					'title': 'Months'
				}
			}
			yAxis = {
				{
					'min': 0.0,
					'max': 1000.0,
					'title': 'Bit Coin Tax?'
				}
			}
		/>
	</Container>
	<Container>

		<BaseChart
			title='A Line Chart With Bars'
			subtitle='This is a Line Chart with bars'
			id='0'

			series = {
				{
					'Speed': {
						'type': 'line',
						'get_data': () => $.post(location.origin + '/data_Speed')
					},
					'Another_Value': {
						'type': 'column',
						'get_data': () => $.post(location.origin + '/data_Another_Value')
					}
				}
			}
			yAxis = {
				{
					'min': 0.0,
					'max': 20.0,
					'title': 'Only Y Axis Defined'
				}
			}
		/>
	</Container>
	<Container>
		<Menu
			style='dropdown'
		
			name='Menu'

			functions = {
				{
					'Scatter': {
						'action': () => $.post(location.origin + '/action_Scatter').done(function_result)
					},
					'Line': {
						'action': () => $.post(location.origin + '/action_Line').done(function_result)
					},
				}
			}
		/>

	</Container>
</Container>;

const scatter_page = <Container>
	<Container>

		<BaseChart
			title='The Scatter Plot'
			subtitle='Made the exactly same way as others charts'
			id='0'

			series = {
				{
					'Bitcoin_Price': {
						'type': 'scatter',
						'get_data': () => $.post(location.origin + '/data_Bitcoin_Price')
					}
				}
			}
			xAxis = {
				{
					'min': 0.0,
					'max': 100.0,
				}
			}
			yAxis = {
				{
					'min': 0.0,
					'max': 100.0,
				}
			}
		/>
	</Container>
</Container>;

const line_page = <Container>
	<Container>

		<BaseChart
			title='A Line Chart'
			subtitle=''
			id='0'

			series = {
				{
					'Money': {
						'type': 'line',
						'get_data': () => $.post(location.origin + '/data_Money')
					},
					'Not_Slept_Hours': {
						'type': 'line',
						'get_data': () => $.post(location.origin + '/data_Not_Slept_Hours')
					}
				}
			}
		/>
	</Container>
	<Container>

		<BaseChart
			title='Another Line Chart'
			subtitle=''
			id='0'

			series = {
				{
					'Not_Slept_Hours': {
						'type': 'line',
						'get_data': () => $.post(location.origin + '/data_Not_Slept_Hours')
					}
				}
			}
			xAxis = {
				{
					'min': 0.0,
					'max': 10.0,
					'title': 'Some Short Range'
				}
			}
			yAxis = {
				{
					'min': 50.0,
					'max': 1500.0,
					'title': 'A Long Range'
				}
			}
		/>
	</Container>
</Container>;

export default class Dashboard extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			main: main_page,
			scatter: scatter_page,
			line: line_page,
		};
	}

	render() {
		return(this.state[this.props.page]);
	}
}
