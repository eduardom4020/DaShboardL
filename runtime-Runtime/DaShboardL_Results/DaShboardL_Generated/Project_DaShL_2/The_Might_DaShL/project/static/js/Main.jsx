var $ = require('jquery');

import React from 'react';
import { Router, Route, hashHistory, Switch } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import Dashboard from './Dashboard';

const newHistory = createBrowserHistory();

const main_page = () => <Dashboard page="main"/>;
const scatter_page = () => <Dashboard page="scatter"/>;
const line_page = () => <Dashboard page="line"/>;

export default class Project extends React.Component {
	constructor(props) {
		super(props);
	}

	render () {
		return (
			<Router history={newHistory}>
				<Switch>
					<Route path='/' component={main_page} />
					<Route path='/scatter' component={scatter_page} />
					<Route path='/line' component={line_page} />
					
				</Switch>
			</Router>
		);
	}
}
