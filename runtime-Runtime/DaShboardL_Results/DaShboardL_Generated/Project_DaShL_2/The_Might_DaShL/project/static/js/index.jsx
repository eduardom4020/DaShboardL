import React from 'react';
import ReactDOM from 'react-dom';
import Main from './Main';

import { Provider } from 'react-redux';

import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(
	<Main />
, document.getElementById('content'));
